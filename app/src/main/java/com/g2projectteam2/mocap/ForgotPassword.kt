package com.g2projectteam2.mocap

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import com.g2projectteam2.mocap.api.APIClient
import com.g2projectteam2.mocap.api.APIInterface
import com.g2projectteam2.mocap.model.PostForgotEmail
import com.g2projectteam2.mocap.model.PostForgotPhone
import com.g2projectteam2.mocap.model.PostRegisterResponse
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register_info.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPassword : AppCompatActivity() {
    companion object{
        const val IDOTP = "id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_forgot_password)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btn_forgot_password_next.setOnClickListener {

            if (et_forgot_password_email.text.toString().isEmpty()) {
                Toast.makeText(this, "Field cannot be empty", Toast.LENGTH_SHORT).show()

            } else {

                if (et_forgot_password_email.text.toString().contains('@')) {
                    val forgotEmailUser =
                                APIClient().getClient()?.create(APIInterface::class.java)
                                    ?.postForgotEmailUser(
                                        PostForgotEmail(
                                            et_forgot_password_email.text.toString()
                                        )
                                    )
                            forgotEmailUser?.enqueue(object : Callback<PostRegisterResponse>{
                                override fun onFailure(call: Call<PostRegisterResponse>, t: Throwable) {
                                    Toast.makeText(
                                        applicationContext,
                                        "Connection Error",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }

                                override fun onResponse(
                                    call: Call<PostRegisterResponse>,
                                    response: Response<PostRegisterResponse>
                                ) {

                                    if (response.isSuccessful) {
                                        Log.d("activate",response.body()?.user.toString())

                                        if(response.body()!!.user!!.activeStatus!!){
                                            val intent = Intent(applicationContext, NewPasswordVerification::class.java)
                                            intent.putExtra(IDOTP, response.body()?.user!!.id)
                                            startActivity(intent)
                                            finish()
                                        }else{
                                            Toast.makeText(
                                                applicationContext,
                                                "Account has not been verified",
                                                Toast.LENGTH_SHORT
                                            ).show()
                                        }


                                    }else{
                                        Toast.makeText(
                                            applicationContext,
                                            "Email or phone is not recognized by system",
                                            Toast.LENGTH_SHORT
                                        ).show()

                                    }
                        }
                    })
                }else{
                    var inputNumber = et_forgot_password_email.text.toString()
                    var inputFix = ""

                    if(inputNumber.first()=='0'){
                        inputNumber = inputNumber.substring(1)
                        inputFix = "+62$inputNumber"
                        Log.d("Input2",inputFix)
                    }else if(inputNumber.first()=='6'){
                        inputFix = "+${inputNumber}"
                        Log.d("Input1",inputFix)
                    }
                    else{
                        inputFix=inputNumber
                        Log.d("Input3",inputFix)
                    }
                    val forgotPhoneUser =
                        APIClient().getClient()?.create(APIInterface::class.java)
                            ?.postForgotPhoneUser(
                                PostForgotPhone(
                                    inputFix
                                )
                            )
                    forgotPhoneUser?.enqueue(object : Callback<PostRegisterResponse>{
                        override fun onFailure(call: Call<PostRegisterResponse>, t: Throwable) {
                            Toast.makeText(
                                applicationContext,
                                "Connection Error",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                        override fun onResponse(
                            call: Call<PostRegisterResponse>,
                            response: Response<PostRegisterResponse>
                        ) {

                            if (response.isSuccessful) {
                                if(response.body()!!.user!!.activeStatus!!){
                                    val intent = Intent(applicationContext, NewPasswordVerification::class.java)
                                    intent.putExtra(IDOTP, response.body()?.user!!.id)
                                    startActivity(intent)
                                    finish()
                                }else{
                                    Toast.makeText(
                                        applicationContext,
                                        "Account has not been verified",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }else{
                                Toast.makeText(
                                    applicationContext,
                                    "Email or phone number is not recognized by system",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    })

                }
            }


        }

        et_forgot_password_email.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s.toString().isEmpty()) {
                    et_forgot_password_email.setError("field cannot be empty")
                    btn_forgot_password_next.isEnabled = false
                } else {
                    et_forgot_password_email.setError(null)
                    btn_forgot_password_next.isEnabled = true
                }
            }

        })
    }
}
