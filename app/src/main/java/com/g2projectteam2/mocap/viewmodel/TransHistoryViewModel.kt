package com.g2projectteam2.mocap.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.g2projectteam2.mocap.database.entity.TransHistoryInProgressEntity
import com.g2projectteam2.mocap.repository.DatabaseRepository

class TransHistoryViewModel(private var databaseRepository: DatabaseRepository): ViewModel() {
    private var allHistory: LiveData<List<TransHistoryInProgressEntity>> = databaseRepository.getAllHistory()

    fun insert(transHistoryInProgressEntity: TransHistoryInProgressEntity){
        databaseRepository.insert(transHistoryInProgressEntity)
    }

    fun delete(transHistoryInProgressEntity: TransHistoryInProgressEntity){
        databaseRepository.delete(transHistoryInProgressEntity)
    }

    fun deleteAllHistory(){
        databaseRepository.deleteAllHistory()
    }

    fun getAllHistory(): LiveData<List<TransHistoryInProgressEntity>> {
        return allHistory
    }
}