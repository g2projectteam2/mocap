package com.g2projectteam2.mocap.di

import com.g2projectteam2.mocap.adapters.TransHistoryInProgressRVAdapter
import com.g2projectteam2.mocap.database.TransHistoryInProgressDatabase
import com.g2projectteam2.mocap.repository.DatabaseRepository
import com.g2projectteam2.mocap.viewmodel.TransHistoryViewModel
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.dsl.module

val dbModule = module {
    single {
        TransHistoryInProgressDatabase.getInstance(
            context = get()
        )
    }
    factory { get<TransHistoryInProgressDatabase>().transHistoryDao() }
}

val repositoryModule = module {
    single { DatabaseRepository(get()) }
}

val uiModule = module {
    factory { TransHistoryInProgressRVAdapter() }
    viewModel { TransHistoryViewModel(get()) }
}