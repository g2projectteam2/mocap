package com.g2projectteam2.mocap

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.g2projectteam2.mocap.api.APIClient
import com.g2projectteam2.mocap.api.APIInterface
import com.g2projectteam2.mocap.model.*
import kotlinx.android.synthetic.main.activity_login_verification_activiry.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response



class LoginVerificationActiviry : AppCompatActivity() {

    companion object {
        const val IDREGLOGIN = "id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login_verification_activiry)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)

        et_email_validate.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s.toString().isEmpty()) {
                    et_email_validate.setError("field cannot be empty")

                } else {
                    et_email_validate.setError(null)
                }
            }

        })

        et_password_validate.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().isEmpty()) {
                    et_password_validate.error = "field cannot be empty"

                } else {
                    et_password_validate.setError(null)

                }
            }

        })

        btn_login_validate.setOnClickListener {
            if (et_email_validate.text.toString().isEmpty() || et_password_validate.text.toString()
                    .isEmpty()
            ) {
                Toast.makeText(this, "Field cannot be empty.", Toast.LENGTH_SHORT).show()
            } else {

                if (et_email_validate.text.toString().contains('@')) {
                    Log.d("tes", "if")
                    val loginEmailUser =
                        APIClient().getClient()?.create(APIInterface::class.java)
                            ?.postLoginEmailUser(
                                PostLoginEmail(
                                    et_email_validate.text.toString(),
                                    et_password_validate.text.toString()
                                )
                            )
                    loginEmailUser?.enqueue(object : Callback<PostRegisterResponse> {
                        override fun onFailure(call: Call<PostRegisterResponse>, t: Throwable) {
                            Toast.makeText(
                                applicationContext,
                                "Connection Error",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                        override fun onResponse(
                            call: Call<PostRegisterResponse>,
                            response: Response<PostRegisterResponse>
                        ) {

                            if (response.isSuccessful) {

                                    Toast.makeText(
                                        applicationContext,
                                        "Account has been verified",
                                        Toast.LENGTH_SHORT
                                    ).show()
                            } else {

                                var jsonObject: JSONObject? = null
                                try {
                                    jsonObject = JSONObject(response.errorBody()!!.string())
                                    Log.d("JSON",jsonObject.toString())
                                    if (jsonObject.getString("description")=="this account has not been activated yet"){
                                        val idOTPValidateEmail = jsonObject.getInt("userId")

                                        val resendOTPUser =
                                            APIClient().getClient()?.create(APIInterface::class.java)
                                                ?.postResendOTPUser(
                                                    PostResendOTP(idOTPValidateEmail)
                                                )
                                        resendOTPUser?.enqueue(object : Callback<ResponseResendOTP> {
                                            override fun onFailure(
                                                call: Call<ResponseResendOTP>,
                                                t: Throwable
                                            ) {
                                                Toast.makeText(
                                                    applicationContext,
                                                    "Connection error",
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                            }

                                            override fun onResponse(
                                                call: Call<ResponseResendOTP>,
                                                response: Response<ResponseResendOTP>
                                            ) {
                                                if (response.isSuccessful) {
                                                    Toast.makeText(
                                                        applicationContext,
                                                        "New code OTP has been sent",
                                                        Toast.LENGTH_SHORT
                                                    ).show()
                                                    val intent = Intent(
                                                        applicationContext,
                                                        RegisterVerification::class.java
                                                    )
                                                    intent.putExtra(IDREGLOGIN, response.body()?.user!!.id)
                                                    startActivity(intent)
                                                    finish()
                                                } else {
                                                    Toast.makeText(
                                                        applicationContext,
                                                        "Connection Error",
                                                        Toast.LENGTH_SHORT
                                                    ).show()
                                                }
                                            }
                                        })
                                    }else{
                                        Toast.makeText(
                                            applicationContext,
                                            "Wrong email or password",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                }
                            }


                        }

                    })
                } else {

                    var inputNumber = et_email_validate.text.toString()
                    var inputFix = ""

                    if (inputNumber.first() == '0') {
                        inputNumber = inputNumber.substring(1)
                        inputFix = "+62$inputNumber"
                        Log.d("Input2", inputFix)
                    } else if (inputNumber.first() == '6') {
                        inputFix = "+${inputNumber}"
                        Log.d("Input1", inputFix)
                    } else {
                        inputFix = inputNumber
                        Log.d("Input3", inputFix)
                    }
                    Log.d("tes", "else")

                    val loginPhoneUser =
                        APIClient().getClient()?.create(APIInterface::class.java)
                            ?.postLoginPhoneUser(
                                PostLoginPhone(
                                    inputFix,
                                    et_password_validate.text.toString()
                                )
                            )
                    loginPhoneUser?.enqueue(object : Callback<PostRegisterResponse> {
                        override fun onFailure(call: Call<PostRegisterResponse>, t: Throwable) {
                            Toast.makeText(
                                applicationContext,
                                "Connection Error",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                        override fun onResponse(
                            call: Call<PostRegisterResponse>,
                            response: Response<PostRegisterResponse>
                        ) {

                            if (response.isSuccessful) {

                                Toast.makeText(
                                    applicationContext,
                                    "Account has been verified",
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {

                                var jsonObject: JSONObject? = null
                                try {
                                    jsonObject = JSONObject(response.errorBody()!!.string())
                                    Log.d("JSON",jsonObject.toString())
                                    if (jsonObject!!.getString("description")=="this account has not been activated yet"){
                                        val idOTPValidateEmail = jsonObject!!.getInt("userId")

                                        val resendOTPUser =
                                            APIClient().getClient()?.create(APIInterface::class.java)
                                                ?.postResendOTPUser(
                                                    PostResendOTP(idOTPValidateEmail)
                                                )
                                        resendOTPUser?.enqueue(object : Callback<ResponseResendOTP> {
                                            override fun onFailure(
                                                call: Call<ResponseResendOTP>,
                                                t: Throwable
                                            ) {
                                                Toast.makeText(
                                                    applicationContext,
                                                    "Connection error",
                                                    Toast.LENGTH_SHORT
                                                ).show()
                                            }

                                            override fun onResponse(
                                                call: Call<ResponseResendOTP>,
                                                response: Response<ResponseResendOTP>
                                            ) {
                                                if (response.isSuccessful) {
                                                    Toast.makeText(
                                                        applicationContext,
                                                        "New code OTP has been sent",
                                                        Toast.LENGTH_SHORT
                                                    ).show()
                                                    val intent = Intent(
                                                        applicationContext,
                                                        RegisterVerification::class.java
                                                    )
                                                    intent.putExtra(IDREGLOGIN, response.body()?.user!!.id)
                                                    startActivity(intent)
                                                    finish()
                                                } else {
                                                    Toast.makeText(
                                                        applicationContext,
                                                        "Connection Error",
                                                        Toast.LENGTH_SHORT
                                                    ).show()
                                                }
                                            }
                                        })
                                    }else{
                                        Toast.makeText(
                                            applicationContext,
                                            "Wrong email or password",
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    }
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                }
                            }


                        }

                    })

                }


            }
        }
    }
}
