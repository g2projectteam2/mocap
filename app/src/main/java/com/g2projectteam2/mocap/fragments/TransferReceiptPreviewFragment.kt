package com.g2projectteam2.mocap.fragments

import android.Manifest
import android.app.Activity.RESULT_OK
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.content.pm.PackageManager.FEATURE_CAMERA_ANY
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.media.ExifInterface
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.provider.MediaStore.ACTION_IMAGE_CAPTURE
import android.provider.MediaStore.EXTRA_OUTPUT
import android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.annotation.RequiresApi
import androidx.core.content.FileProvider
import androidx.core.graphics.drawable.toBitmap
import androidx.fragment.app.Fragment
import com.g2projectteam2.mocap.LoginActivity
import com.g2projectteam2.mocap.LoginActivity.Companion.PHONE
import com.g2projectteam2.mocap.MainActivity
import com.g2projectteam2.mocap.MainActivity.Companion.BUNDLE_DATA_USER
import com.g2projectteam2.mocap.R
import com.g2projectteam2.mocap.api.APIClient
import com.g2projectteam2.mocap.api.APIInterface
import com.g2projectteam2.mocap.database.entity.TransHistoryInProgressEntity
import com.g2projectteam2.mocap.fragments.CreditMainFragment.Companion.INFO_DATA
import com.g2projectteam2.mocap.fragments.CreditMainFragment.Companion.TRANS_IN_PROGRESS
import com.g2projectteam2.mocap.models.PostCancelTrans
import com.g2projectteam2.mocap.models.PostValidateTrans
import com.g2projectteam2.mocap.models.PostValidateTransResModel
import com.g2projectteam2.mocap.models.TransHistoryInProgressModel
import com.g2projectteam2.mockup.models.InfoFragmentModel
import kotlinx.android.synthetic.main.fragment_transfer_receipt_preview.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileNotFoundException
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class TransferReceiptPreviewFragment : Fragment() {

    companion object {
        private const val CAMERA_REQUEST = 101
        private const val GALLERY_REQUEST = 102
    }

    private var pathImageSave = ""

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_transfer_receipt_preview, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        iv_receipt_preview.setImageResource(R.drawable.ic_photo_camera)
        iv_receipt_preview.tag = R.drawable.ic_photo_camera

        checkPermission()

        btn_take_photo.setOnClickListener {
            intentToCapture()
        }

        btn_upload.setOnClickListener {
            if (iv_receipt_preview.tag != R.drawable.ic_photo_camera) {
                val imagePreview = iv_receipt_preview.drawable.toBitmap()
                val byteArrayOutputStream = ByteArrayOutputStream()
                imagePreview.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream)
                val byteArray: ByteArray = byteArrayOutputStream.toByteArray()
                val encodedImage: String = Base64.getEncoder().encodeToString(byteArray)

                val sharedPreferences =
                    context?.getSharedPreferences(BUNDLE_DATA_USER, Context.MODE_PRIVATE)
                val userPhoneNumber = sharedPreferences?.getString(PHONE, "")

//                bg_grey_receipt.visibility = View.VISIBLE
                pb_receipt.visibility = View.VISIBLE

                if (userPhoneNumber!!.isNotEmpty()) {
                    APIClient().getClient()?.create(APIInterface::class.java)?.postValidateTrans(
                        PostValidateTrans(userPhoneNumber, encodedImage)
                    )?.enqueue(object : Callback<PostValidateTransResModel> {
                        override fun onFailure(
                            call: Call<PostValidateTransResModel>,
                            t: Throwable
                        ) {
//                            bg_grey_receipt.visibility = View.GONE
                            pb_receipt.visibility = View.GONE
                            Toast.makeText(
                                context,
                                "Something wrong with server.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                        override fun onResponse(
                            call: Call<PostValidateTransResModel>,
                            response: Response<PostValidateTransResModel>
                        ) {
//                            bg_grey_receipt.visibility = View.GONE
                            pb_receipt.visibility = View.GONE
                            if (response.isSuccessful) {
                                if (arguments != null) {
                                    val inProgressTrans =
                                        arguments!!.getParcelable<TransHistoryInProgressModel>(
                                            TRANS_IN_PROGRESS
                                        )
                                    if (inProgressTrans != null) {
                                        val phoneNumber =
                                            if (inProgressTrans.phoneNumber.substring(0..2) == "+62") {
                                                inProgressTrans.phoneNumber
                                            } else {
                                                "+62${inProgressTrans.phoneNumber}"
                                            }
                                        val infoModel =
                                            InfoFragmentModel(
                                                "PAYMENT SUCCESS",
                                                "You bought :", inProgressTrans.product,
                                                "To :", phoneNumber,
                                                CreditMainFragment.OK
                                            )
                                        showInfoFragment(infoModel, inProgressTrans)

                                        val transHistoryViewModel = MainActivity().getHistoryMView()
                                        val transData = TransHistoryInProgressEntity(
                                            inProgressTrans.phoneNumber,
                                            inProgressTrans.date,
                                            inProgressTrans.product,
                                            inProgressTrans.totalPayment
                                        )
                                        val id = inProgressTrans.id
                                        transData.id = id
                                        transHistoryViewModel.delete(transData)
                                    }
                                }
                            } else {
                                val jsonObject: JSONObject?
                                try {
                                    jsonObject = JSONObject(response.errorBody()!!.string())
                                    Toast.makeText(
                                        context,
                                        jsonObject.getString("description").capitalize(),
                                        Toast.LENGTH_SHORT
                                    ).show()
                                    Log.d("TestingProof", jsonObject.toString())
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                }

                            }
                        }
                    })

                } else {
                    Toast.makeText(
                        context,
                        "Account didn't properly sign in. Please retry it.",
                        Toast.LENGTH_SHORT
                    ).show()
                }
            } else {
                Toast.makeText(
                    context,
                    "Sorry you can't proceed. You haven't select any photo.",
                    Toast.LENGTH_SHORT
                ).show()
            }

        }

        btn_chose_from_gallery.setOnClickListener {
            val intent = Intent().setType("image/*")
            intent.action = Intent.ACTION_GET_CONTENT
            startActivityForResult(Intent.createChooser(intent, "Select Picture"), GALLERY_REQUEST)
        }

        tv_back_receipt_preview.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }
    }

    private fun showInfoFragment(
        infoModel: InfoFragmentModel,
        transInProgress: TransHistoryInProgressModel
    ) {
        val fragmentInfo = InfoFragment()
        val bundle = Bundle()
        val fragmentTransaction = activity?.supportFragmentManager?.beginTransaction()

        bundle.putParcelable(INFO_DATA, infoModel)
        bundle.putParcelable(TRANS_IN_PROGRESS, transInProgress)
        fragmentInfo.arguments = bundle
        fragmentInfo.show(fragmentTransaction!!, "infofragment")
    }

    private fun intentToCapture() {
        if (context?.packageManager?.hasSystemFeature(FEATURE_CAMERA_ANY)!!) {
            val intent = Intent(ACTION_IMAGE_CAPTURE)
            if (intent.resolveActivity(context?.packageManager!!) != null) {
                var photoFile: File? = null
                try {
                    photoFile = createImageFile()
                } catch (e: IOException) {
                    e.printStackTrace()
                }

                if (photoFile != null) {
                    val uriFromFile =
                        FileProvider.getUriForFile(
                            context!!,
                            context?.packageName + ".provider",
                            photoFile
                        )
                    intent.putExtra(EXTRA_OUTPUT, uriFromFile)
                    startActivityForResult(intent, CAMERA_REQUEST)
                }
            }
        } else {
            Toast.makeText(context, "Please check your camera functionality", Toast.LENGTH_SHORT)
                .show()
        }
    }

    private fun createImageFile(): File {
        val currentTime =
            Calendar.getInstance(TimeZone.getDefault()).time
        val dateFormat = SimpleDateFormat("dd-MM-yyy_hh-mm-ss", Locale.getDefault())
        val fileName = "bukti-transfer_${dateFormat.format(currentTime)}"
        val fileDir = context?.getExternalFilesDir(Environment.DIRECTORY_DCIM)

        val image = File.createTempFile(fileName, ".jpg", fileDir)
        pathImageSave = image.absolutePath

        return image
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == CAMERA_REQUEST) {
            val imageFile = File(pathImageSave)
            if (resultCode == RESULT_OK) {
                if (imageFile.exists()) {
                    val bitmap = BitmapFactory.decodeFile(imageFile.absolutePath)
                    val rotatedBitmap = imageRotator(bitmap, pathImageSave)
                    val imageUri = Uri.fromFile(imageFile)

                    val currentTime = Calendar.getInstance(TimeZone.getDefault())
                        .time
                    val dateFormat = SimpleDateFormat("dd-MM-yyy_hh-mm-ss", Locale.getDefault())
                    val fileName = "bukti-transfer_${dateFormat.format(currentTime)}.jpg"

                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
                        val relativePath =
                            Environment.DIRECTORY_PICTURES + File.separator + "MocapApp"

                        val contentValues = ContentValues()
                        contentValues.put(MediaStore.MediaColumns.DISPLAY_NAME, fileName)
                        contentValues.put(MediaStore.MediaColumns.MIME_TYPE, "images/jpg")
                        contentValues.put(MediaStore.MediaColumns.RELATIVE_PATH, relativePath)

                        val uri =
                            context?.contentResolver?.insert(EXTERNAL_CONTENT_URI, contentValues)
                        try {
                            val outputStream = context?.contentResolver?.openOutputStream(uri!!)
                            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, outputStream)
                            outputStream?.close()
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    } else {
                        Log.d("Testing", "masuk sini")
                        val arrayString = arrayOf(imageFile.absolutePath)
                        MediaScannerConnection.scanFile(context, arrayString, arrayOf("images/*"),
                            object : MediaScannerConnection.OnScanCompletedListener {
                                override fun onScanCompleted(path: String?, uri: Uri?) {
                                    Toast.makeText(context, "ini jalan", Toast.LENGTH_SHORT).show()
                                }

                            })
                    }

                    iv_receipt_preview.setImageBitmap(rotatedBitmap)
                    iv_receipt_preview.tag = R.drawable.ic_phone
                }
            } else {
                Toast.makeText(context, "Please take a photo.", Toast.LENGTH_SHORT).show()
            }

            imageFile.delete()
        }

        if (requestCode == GALLERY_REQUEST) {
            try {
                if (data != null) {
                    val imageUri = data.data!!

                    val bitmap = getBitmapFromUri(imageUri)

                    iv_receipt_preview.setImageBitmap(bitmap)
                    iv_receipt_preview.tag = R.drawable.ic_phone
                } else {
                    Toast.makeText(context, "You haven't choose a picture.", Toast.LENGTH_SHORT)
                        .show()
                }

            } catch (e: FileNotFoundException) {
                e.printStackTrace()
                Toast.makeText(context, "File not found in your device.", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun imageRotator(bitmap: Bitmap, path: String): Bitmap {
        var rotate = 0
        val exif = ExifInterface(path)

        when (exif.getAttributeInt(
            ExifInterface.TAG_ORIENTATION,
            ExifInterface.ORIENTATION_NORMAL
        )) {
            ExifInterface.ORIENTATION_ROTATE_90 -> rotate = 90
            ExifInterface.ORIENTATION_ROTATE_180 -> rotate = 180
            ExifInterface.ORIENTATION_ROTATE_270 -> rotate = 270
        }

        val matrix = Matrix()
        matrix.postRotate(rotate.toFloat())
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.width, bitmap.height, matrix, true)
    }

    private fun getBitmapFromUri(uri: Uri): Bitmap {
        val parcelFileDescriptor = activity?.contentResolver?.openFileDescriptor(uri, "r")
        val fileDescriptor = parcelFileDescriptor?.fileDescriptor
        val bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor)
        parcelFileDescriptor?.close()
        return bitmap
    }

    private fun checkPermission() {
        if (context?.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
            context?.checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
        ) {
            requestPermissions(
                arrayOf(
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
                ), 0
            )
        }
    }

}
