package com.g2projectteam2.mocap.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.recyclerview.widget.LinearLayoutManager
import com.g2projectteam2.mocap.LoginActivity.Companion.ID
import com.g2projectteam2.mocap.MainActivity.Companion.BUNDLE_DATA_USER
import com.g2projectteam2.mocap.R
import com.g2projectteam2.mocap.adapters.TransHistoryCompletedRVAdapter
import com.g2projectteam2.mocap.api.APIClient
import com.g2projectteam2.mocap.api.APIInterface
import com.g2projectteam2.mocap.models.GetTransHistoryResModel
import kotlinx.android.synthetic.main.fragment_completed.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class CompletedFragment : Fragment() {

    private var recycleViewAdapter: TransHistoryCompletedRVAdapter? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_completed, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sharedPreferences = context?.getSharedPreferences(BUNDLE_DATA_USER, Context.MODE_PRIVATE)
        val userId = sharedPreferences?.getString(ID, null)

        rv_completed_history.layoutManager = LinearLayoutManager(context)
        recycleViewAdapter = TransHistoryCompletedRVAdapter()
        getHistoryData(userId)
    }

    private fun getHistoryData(userId: String?){
        if (userId != null) {
            APIClient().getClient()?.create(APIInterface::class.java)
                ?.getTransHistory(userId.toInt())?.enqueue(object : Callback<GetTransHistoryResModel> {
                    override fun onFailure(call: Call<GetTransHistoryResModel>, t: Throwable) {
                        Toast.makeText(context, "Connection issue.", Toast.LENGTH_SHORT)
                            .show()
                    }

                    override fun onResponse(
                        call: Call<GetTransHistoryResModel>,
                        response: Response<GetTransHistoryResModel>
                    ) {
                        if (response.isSuccessful) {
                            Log.d("TestingComplete", "masuk sini")
                            recycleViewAdapter?.setDataList(response.body()!!.transactions.reversed())
                            rv_completed_history.adapter = recycleViewAdapter
                        } else {
                            var jsonObject: JSONObject?
                            try {
                                jsonObject = JSONObject(response.errorBody()!!.string())
                            }catch (e: JSONException){
                                e.printStackTrace()
                            }
                            Toast.makeText(
                                context,
                                "History transaction is missing.",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                })
        } else {
            Toast.makeText(context, "Account didn't properly sign in. Please retry to login.", Toast.LENGTH_SHORT).show()
        }
    }

}
