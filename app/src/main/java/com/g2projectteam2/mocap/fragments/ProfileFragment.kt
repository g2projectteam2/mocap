package com.g2projectteam2.mocap.fragments

import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import com.g2projectteam2.mocap.LoginActivity
import com.g2projectteam2.mocap.LoginActivity.Companion.BALANCE
import com.g2projectteam2.mocap.LoginActivity.Companion.EMAIL
import com.g2projectteam2.mocap.LoginActivity.Companion.PHONE
import com.g2projectteam2.mocap.LoginActivity.Companion.PREFERENCE
import com.g2projectteam2.mocap.MainActivity.Companion.BUNDLE_DATA_USER
import com.g2projectteam2.mocap.R
import com.g2projectteam2.mocap.SplashActivity
import com.google.gson.Gson
import kotlinx.android.synthetic.main.fragment_profile.*
import java.util.*

class ProfileFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_profile, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val preferences = context?.getSharedPreferences(BUNDLE_DATA_USER, Context.MODE_PRIVATE)
        tv_email_profile.text = preferences?.getString(EMAIL, "")?.toUpperCase()
        val userPhone = preferences?.getString(PHONE, "")
        if (userPhone != "") {
            tv_number_profile.text = userPhone?.substring(0, (userPhone.length - 4)) + "****"
        }

        val balance = preferences?.getString(BALANCE, "")
        if (balance != "") {
            when (balance?.length) {
                4 -> tv_balance_profile.text =
                    "Rp${balance.first()}.${balance.subSequence(0..balance.length - 1)}"
                5 -> tv_balance_profile.text =
                    "Rp${balance.subSequence(0, 2)}.${balance.subSequence(2..balance.length - 1)}"
                6 -> tv_balance_profile.text =
                    "Rp${balance.subSequence(0, 3)}.${balance.subSequence(3..balance.length - 1)}"
                7 -> tv_balance_profile.text =
                    "Rp${balance.first()}.${balance.subSequence(0..2)}." +
                            "${balance.subSequence(2 until balance.length - 1)}"
            }
        }

        tv_back_profile.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
        }

        btn_logout_profile.setOnClickListener {
            val alertDialog = AlertDialog.Builder(context!!).apply {
                setTitle("Are you sure want to logout?")
                setCancelable(true)
                setPositiveButton("Yes") { _, _ ->
                    val prefs = context?.getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
                    val editor = prefs?.edit()
                    editor?.putBoolean(PREFERENCE, false)
                    editor?.apply()
                    startActivity(Intent(context, SplashActivity::class.java))
                    activity?.finish()
                }
                setNegativeButton("No") { dialog, _ ->
                    dialog.dismiss()
                }
            }.create()
            alertDialog.setOnShowListener {
                alertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(resources.getColor(R.color.red))
            }
            alertDialog.show()
        }
    }

}
