package com.g2projectteam2.mocap.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.g2projectteam2.mocap.MainActivity
import com.g2projectteam2.mocap.R
import com.g2projectteam2.mocap.adapters.TransHistoryInProgressRVAdapter
import com.g2projectteam2.mocap.database.entity.TransHistoryInProgressEntity
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.fragment_in_progress.*
import java.lang.reflect.Type

class InProgressFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_in_progress, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //TODO implement delete button for every item

        val adapter = MainActivity().getAdapterTransHistory()
        val historyViewModel = MainActivity().getHistoryMView()

        rv_in_progress_history.layoutManager = LinearLayoutManager(context)
        val recycleViewAdapter = adapter
        rv_in_progress_history.adapter = recycleViewAdapter
        historyViewModel.getAllHistory().observe(viewLifecycleOwner, Observer { list ->
            list.let {
                adapter.setDataList(ArrayList(it))
            }
        })
    }
}
