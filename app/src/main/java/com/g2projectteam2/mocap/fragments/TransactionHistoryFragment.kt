package com.g2projectteam2.mocap.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.g2projectteam2.mocap.R
import com.g2projectteam2.mocap.adapters.TransactionHistoryVPAdapter
import kotlinx.android.synthetic.main.fragment_profile.*
import kotlinx.android.synthetic.main.fragment_transaction_history.*

class TransactionHistoryFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_transaction_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        history_view_pager.adapter = TransactionHistoryVPAdapter(childFragmentManager)
        history_tab_layout.setupWithViewPager(history_view_pager)

        tv_back_trans_history.setOnClickListener {
            activity?.supportFragmentManager?.popBackStack()
            childFragmentManager.popBackStack()
        }
    }

}
