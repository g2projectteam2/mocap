package com.g2projectteam2.mocap.fragments

import android.content.Context.MODE_PRIVATE
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Adapter
import android.widget.LinearLayout
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.GridLayoutManager
import com.bumptech.glide.Glide
import com.g2projectteam2.mocap.LoginActivity.Companion.BALANCE
import com.g2projectteam2.mocap.LoginActivity.Companion.PHONE
import com.g2projectteam2.mocap.MainActivity
import com.g2projectteam2.mocap.MainActivity.Companion.BUNDLE_DATA_USER
import com.g2projectteam2.mocap.R
import com.g2projectteam2.mocap.adapters.MobileCreditRVAdapter
import com.g2projectteam2.mocap.adapters.TransHistoryInProgressRVAdapter.Companion.IS_FROM_HISTORY
import com.g2projectteam2.mocap.api.APIClient
import com.g2projectteam2.mocap.api.APIInterface
import com.g2projectteam2.mocap.database.entity.TransHistoryInProgressEntity
import com.g2projectteam2.mocap.models.*
import com.g2projectteam2.mockup.models.InfoFragmentModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.android.synthetic.main.bottom_sheet_confirm.*
import kotlinx.android.synthetic.main.fragment_credit_main.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.Type
import java.text.SimpleDateFormat
import java.util.*

class CreditMainFragment : Fragment() {

    private var prefixDetected = false
    private var inputtedPhoneNumber = ""

    companion object {
        const val NEXT = "NEXT"
        const val OK = "OK"
        const val INFO_DATA = "infodata"
        const val TRANS_PENDING_DATA = "TRANS_PENDING_DATA"
        const val DATA_LIST_TRANS = "DATA_LIST_TRANS"
        const val TRANS_IN_PROGRESS = "TRANS_IN_PROGRESS"
        const val AMOUNT_TRANS_SUCCESS = "amounttranssuccess"
    }

    private var transData: TransHistoryInProgressModel? = null
    private var isFromHistory = false
    private var bottomSheetBehavior: BottomSheetBehavior<LinearLayout>? = null
    private var provider = ""
    private var amount = 0
    private var dataId = -1
    private var recycleViewAdapter: MobileCreditRVAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val prefs = context?.getSharedPreferences(
            TRANS_PENDING_DATA, MODE_PRIVATE
        )
        if (prefs?.getBoolean(IS_FROM_HISTORY, false) != null) {
            isFromHistory = prefs.getBoolean(IS_FROM_HISTORY, false)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_credit_main, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sharedPreferences = context?.getSharedPreferences(BUNDLE_DATA_USER, MODE_PRIVATE)
        val userPhone = sharedPreferences?.getString(PHONE, null)
        val balance = sharedPreferences?.getString(BALANCE, null)

        if (userPhone != null && balance!! >= "0") {
            setBalance(balance)
            val phoneNumberLength: Boolean
            if (userPhone.first() == '+') {
                phoneNumberLength = (userPhone.length in 12..15)
                if (phoneNumberLength) {
                    et_phone_number_main.setText(userPhone.substring(3))
                    providerCodeDetection(userPhone.substring(3))
                }
            } else {
                phoneNumberLength = (userPhone.length in 10..13)
                if (phoneNumberLength) {
                    et_phone_number_main.setText(userPhone.substring(1))
                    providerCodeDetection(userPhone.substring(1))
                }
            }
        }

        rv_mobile_credit.layoutManager = GridLayoutManager(context, 2)
        recycleViewAdapter = MobileCreditRVAdapter()

        et_phone_number_main.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()) {
                    val phoneNumberLength = (("+62$s").length in 12..15)
                    if (phoneNumberLength) {
                        providerCodeDetection(s)
                    } else {
                        et_phone_number_main.error = "Phone number unidentified."
                        iv_logo_provider.setImageResource(R.color.transparent)
                        rv_mobile_credit.visibility = View.GONE
                        prefixDetected = false
                    }
                } else {
                    et_phone_number_main.error = "This field must not be empty."
                    iv_logo_provider.setImageResource(R.color.transparent)
                    rv_mobile_credit.visibility = View.GONE
                    prefixDetected = false
                }
            }
        })

        bottomSheetBehavior = BottomSheetBehavior.from(bottom_sheet_confirm)
        bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_HIDDEN

        iv_close_bottom_sheet.setOnClickListener {
            bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_HIDDEN
            rg_payment_method.clearCheck()
        }

        bottomSheetBehavior!!.addBottomSheetCallback(object :
            BottomSheetBehavior.BottomSheetCallback() {
            override fun onSlide(bottomSheet: View, slideOffset: Float) {}
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                    bg_grey.visibility = View.GONE
                } else if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                    bg_grey.visibility = View.VISIBLE
                }
            }
        })

        bg_grey.setOnClickListener {
            bottomSheetBehavior!!.state = BottomSheetBehavior.STATE_HIDDEN
        }

        cv_pay_now.setOnClickListener {
            val radioBtnId = rg_payment_method.checkedRadioButtonId
            val amountPay = tv_amount_pay.text.toString()
            amount = amountPay.substring(2, amountPay.length).toInt()
            pb_main.visibility = View.VISIBLE
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_HIDDEN
            if (radioBtnId != -1) {
                val sharedPreferencesBalance =
                    context?.getSharedPreferences(BUNDLE_DATA_USER, MODE_PRIVATE)
                val currentBalance = sharedPreferencesBalance?.getString(BALANCE, null)?.toInt()

                val bottomSheet = activity?.bottom_sheet_confirm
                val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet!!)
                bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED

                val currentTime =
                    Calendar.getInstance(TimeZone.getDefault()).time
                val dateFormat = SimpleDateFormat("dd-MM-yyy", Locale.getDefault())
                val historyViewModel = MainActivity().getHistoryMView()
                val transHistoryData = TransHistoryInProgressEntity(
                    "+62${et_phone_number_main.text}", dateFormat.format(currentTime),
                    tv_amount_credit.text.toString(), tv_amount_pay.text.toString()
                )

                if (!isFromHistory) {
                    historyViewModel.insert(transHistoryData)
                    getEntityId(transHistoryData)
                }

                val prefs = context?.getSharedPreferences(
                    TRANS_PENDING_DATA, MODE_PRIVATE
                )
                prefs?.edit()?.putBoolean(IS_FROM_HISTORY, false)?.apply()

                if (radioBtnId == R.id.rb_direct) {
                    val postTransData = PostTransModel(
                        userPhone!!,
                        provider,
                        "+62${et_phone_number_main.text}",
                        (tv_amount_pay.text.substring(2, tv_amount_pay.text.length).toInt())
                    )
                    if (isFromHistory){
                        cancelPreviousTransaction(userPhone)
                        Log.d("TestingMainDirect", "masuk sini")
                        Log.d("TestingMainDirect", isFromHistory.toString())
                    }
                    APIClient().getClient()?.create(APIInterface::class.java)
                        ?.postTransBank(postTransData)
                        ?.enqueue(object : Callback<PostTransBankResModel> {
                            override fun onFailure(
                                call: Call<PostTransBankResModel>,
                                t: Throwable
                            ) {
                                pb_main.visibility = View.GONE
                                Toast.makeText(
                                    context,
                                    "Connection issue.",
                                    Toast.LENGTH_SHORT
                                )
                                    .show()
                            }

                            override fun onResponse(
                                call: Call<PostTransBankResModel>,
                                response: Response<PostTransBankResModel>
                            ) {
                                pb_main.visibility = View.GONE
                                if (response.isSuccessful) {
                                    bottomSheetBehavior!!.state =
                                        BottomSheetBehavior.STATE_HIDDEN

                                    val generatedVA = response.body()?.vaNumber
                                    rg_payment_method.clearCheck()
                                    val infoModel =
                                        InfoFragmentModel(
                                            "DIRECT TRANSFER",
                                            "Virtual account number :",
                                            generatedVA!!,
                                            "Amount to pay :",
                                            tv_amount_pay.text.toString(),
                                            NEXT
                                        )
                                    if (transData != null) {
                                        showInfoFragment(infoModel, transData!!)
                                    } else {
                                        val newTransData = TransHistoryInProgressModel(
                                            transHistoryData.phoneNumber,
                                            transHistoryData.date,
                                            transHistoryData.product,
                                            transHistoryData.totalPayment, dataId
                                        )//TODO cari tau cara dapet ID nya
                                        showInfoFragment(infoModel, newTransData)
                                    }
                                } else {
                                    val jsonObject: JSONObject?
                                    try {
                                        jsonObject = JSONObject(response.errorBody()!!.string())
                                        val error = jsonObject.getString("description")
                                        Toast.makeText(
                                            context,
                                            error.capitalize(),
                                            Toast.LENGTH_SHORT
                                        ).show()
                                    } catch (e: JSONException) {
                                        e.printStackTrace()
                                    }
                                }
                            }
                        })
                } else {
                    if (currentBalance != null && currentBalance.toInt() != 0) {
                        val postTransData = PostTransModel(
                            userPhone!!,
                            provider,
                            "+62${et_phone_number_main.text}",
                            (tv_amount_pay.text.substring(2, tv_amount_pay.text.length).toInt())
                        )

                        Log.d("TestingMain", userPhone)
                        Log.d("TestingMain", provider)
                        Log.d("TestingMain", "+62${et_phone_number_main.text}")
                        Log.d("TestingMain", (tv_amount_pay.text.substring(2, tv_amount_pay.text.length).toInt().toString()))

                        if (isFromHistory){
                            cancelPreviousTransaction(userPhone)
                            Log.d("TestingMain", "masuk sini")
                            Log.d("TestingMain", isFromHistory.toString())
                        }
                        Log.d("TestingMain2", isFromHistory.toString())

                        APIClient().getClient()?.create(APIInterface::class.java)
                            ?.postTransInApp(postTransData)
                            ?.enqueue(object : Callback<PostTransInAppResModel> {
                                override fun onFailure(
                                    call: Call<PostTransInAppResModel>,
                                    t: Throwable
                                ) {
                                    pb_main.visibility = View.GONE
                                    Toast.makeText(
                                        context,
                                        "Connection issue.",
                                        Toast.LENGTH_SHORT
                                    )
                                        .show()
                                }

                                override fun onResponse(
                                    call: Call<PostTransInAppResModel>,
                                    response: Response<PostTransInAppResModel>
                                ) {
                                    pb_main.visibility = View.GONE
                                    if (response.isSuccessful) {
                                        bottomSheetBehavior!!.state =
                                            BottomSheetBehavior.STATE_HIDDEN
                                        rg_payment_method.clearCheck()
                                        val infoModel =
                                            InfoFragmentModel(
                                                "PAYMENT SUCCESS",
                                                "You bought :",
                                                tv_amount_credit.text.toString(),
                                                "To :",
                                                "+62${et_phone_number_main.text}",
                                                OK
                                            )
                                        if (transData != null) {
                                            showInfoFragment(infoModel, transData!!)
                                            val transHistoryViewModel =
                                                MainActivity().getHistoryMView()
                                            val transInProgressData =
                                                TransHistoryInProgressEntity(
                                                    transData!!.phoneNumber,
                                                    transData!!.date,
                                                    transData!!.product,
                                                    transData!!.totalPayment
                                                )
                                            val id = transData!!.id
                                            transInProgressData.id = id
                                            transHistoryViewModel.delete(transInProgressData)
                                        } else {
                                            val newTransData = TransHistoryInProgressModel(
                                                et_phone_number_main.text.toString(),
                                                dateFormat.format(currentTime),
                                                tv_amount_credit.text.toString(),
                                                "Rp${tv_amount_pay.text}", dataId
                                            )
                                            showInfoFragment(infoModel, newTransData)
                                        }
                                        context?.getSharedPreferences(
                                            AMOUNT_TRANS_SUCCESS,
                                            MODE_PRIVATE
                                        )
                                            ?.edit()?.putInt(BALANCE, response.body()!!.amount)
                                            ?.apply()
                                        updateBalance()
                                    } else {
                                        val jsonObject: JSONObject?
                                        try {
                                            jsonObject =
                                                JSONObject(response.errorBody()!!.string())
                                            Log.d("TestingMain", jsonObject.toString())
                                            val error = jsonObject.getString("description")
                                            Toast.makeText(
                                                context,
                                                error.capitalize(),
                                                Toast.LENGTH_SHORT
                                            ).show()
                                        } catch (e: JSONException) {
                                            e.printStackTrace()
                                        }

                                    }
                                }
                            })
//                        }
                    } else {
                        Toast.makeText(
                            context, "Cannot proceed transaction. Insufficient balance.",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                }
            } else {
                Toast.makeText(context, "Please choose the payment method", Toast.LENGTH_SHORT)
                    .show()
            }
        }

        processBottomNav()

    }

    private fun getEntityId(transHistoryInProgressEntity: TransHistoryInProgressEntity) {
        val transHistoryViewModel = MainActivity().getHistoryMView()
        transHistoryViewModel.getAllHistory()
            .observe(viewLifecycleOwner, androidx.lifecycle.Observer {
                var index = -1
                it.forEach { it2 ->
                    if (it2 == transHistoryInProgressEntity) {
                        index = it2.id
                    }
                }
                setDataID(index)
            })
    }

    private fun setDataID(index: Int) {
        dataId = index
    }

    private fun providerCodeDetection(prefix: CharSequence) {
        val listCodeXL = context?.resources?.getStringArray(R.array.code_xl)
        val listCodeTelkomsel = context?.resources?.getStringArray(R.array.code_telkomsel)
        val listCodeTri = context?.resources?.getStringArray(R.array.code_tri)
        val listCodeIndosat = context?.resources?.getStringArray(R.array.code_indosat)
        val listCodeSmartfren = context?.resources?.getStringArray(R.array.code_smartfren)
        val listCodeAxis = context?.resources?.getStringArray(R.array.code_axis)
        inputtedPhoneNumber = prefix.toString()
        if (!prefixDetected) {
            val listCreditCall = APIClient().getClient()?.create(APIInterface::class.java)
            pb_main.visibility = View.VISIBLE
            when ("+62" + prefix.subSequence(0..2).toString()) {
                in listCodeXL!! -> {
                    listCreditCall?.getListCreditXL()
                        ?.enqueue(object : Callback<GetListCreditResponse> {
                            override fun onFailure(
                                call: Call<GetListCreditResponse>,
                                t: Throwable
                            ) {
                                pb_main.visibility = View.GONE
                                Toast.makeText(
                                    context,
                                    "System is not responding. Please try again.",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }

                            override fun onResponse(
                                call: Call<GetListCreditResponse>,
                                response: Response<GetListCreditResponse>
                            ) {
                                pb_main.visibility = View.GONE
                                if (response.isSuccessful) {
                                    setupGridRecycleView(response.body()?.amounts!!)
                                } else {
                                    Toast.makeText(
                                        context,
                                        "Provider unrecognized",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }

                        })
                    Glide.with(context!!).load(R.drawable.ic_logo_xl)
                        .override(100, 100)
                        .into(iv_logo_provider)
                    rv_mobile_credit.visibility = View.VISIBLE
                    prefixDetected = true
                    provider = "xl"
                }
                in listCodeTelkomsel!! -> {
                    listCreditCall?.getListCreditTelkomsel()
                        ?.enqueue(object : Callback<GetListCreditResponse> {
                            override fun onFailure(
                                call: Call<GetListCreditResponse>,
                                t: Throwable
                            ) {
                                pb_main.visibility = View.GONE
                                Toast.makeText(
                                    context,
                                    "System is not responding. Please try again.",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }

                            override fun onResponse(
                                call: Call<GetListCreditResponse>,
                                response: Response<GetListCreditResponse>
                            ) {
                                pb_main.visibility = View.GONE
                                if (response.isSuccessful) {
                                    setupGridRecycleView(response.body()?.amounts!!)
                                } else {
                                    Toast.makeText(
                                        context,
                                        "Provider unrecognized",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }

                        })
                    Glide.with(context!!).load(R.drawable.ic_logo_telkomsel)
                        .override(100, 100)
                        .into(iv_logo_provider)
                    rv_mobile_credit.visibility = View.VISIBLE
                    prefixDetected = true
                    provider = "telkomsel"
                }
                in listCodeTri!! -> {
                    listCreditCall?.getListCreditTri()
                        ?.enqueue(object : Callback<GetListCreditResponse> {
                            override fun onFailure(
                                call: Call<GetListCreditResponse>,
                                t: Throwable
                            ) {
                                pb_main.visibility = View.GONE
                                Toast.makeText(
                                    context,
                                    "System is not responding. Please try again.",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }

                            override fun onResponse(
                                call: Call<GetListCreditResponse>,
                                response: Response<GetListCreditResponse>
                            ) {
                                pb_main.visibility = View.GONE
                                if (response.isSuccessful) {
                                    setupGridRecycleView(response.body()?.amounts!!)
                                } else {
                                    Toast.makeText(
                                        context,
                                        "Provider unrecognized",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }

                        })
                    Glide.with(context!!).load(R.drawable.ic_logo_tri)
                        .override(100, 100)
                        .into(iv_logo_provider)
                    rv_mobile_credit.visibility = View.VISIBLE
                    prefixDetected = true
                    provider = "tri"
                }
                in listCodeIndosat!! -> {
                    listCreditCall?.getListCreditIndosat()
                        ?.enqueue(object : Callback<GetListCreditResponse> {
                            override fun onFailure(
                                call: Call<GetListCreditResponse>,
                                t: Throwable
                            ) {
                                pb_main.visibility = View.GONE
                                Toast.makeText(
                                    context,
                                    "System is not responding. Please try again.",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }

                            override fun onResponse(
                                call: Call<GetListCreditResponse>,
                                response: Response<GetListCreditResponse>
                            ) {
                                pb_main.visibility = View.GONE
                                if (response.isSuccessful) {
                                    setupGridRecycleView(response.body()?.amounts!!)
                                } else {
                                    Toast.makeText(
                                        context,
                                        "Provider unrecognized",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }

                        })
                    Glide.with(context!!).load(R.drawable.ic_logo_indosat)
                        .override(100, 100)
                        .into(iv_logo_provider)
                    rv_mobile_credit.visibility = View.VISIBLE
                    prefixDetected = true
                    provider = "indosat"
                }
                in listCodeSmartfren!! -> {
                    listCreditCall?.getListCreditSmartfren()
                        ?.enqueue(object : Callback<GetListCreditResponse> {
                            override fun onFailure(
                                call: Call<GetListCreditResponse>,
                                t: Throwable
                            ) {
                                pb_main.visibility = View.GONE
                                Toast.makeText(
                                    context,
                                    "System is not responding. Please try again.",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }

                            override fun onResponse(
                                call: Call<GetListCreditResponse>,
                                response: Response<GetListCreditResponse>
                            ) {
                                pb_main.visibility = View.GONE
                                if (response.isSuccessful) {
                                    setupGridRecycleView(response.body()?.amounts!!)
                                } else {
                                    Toast.makeText(
                                        context,
                                        "Provider unrecognized",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }

                        })
                    Glide.with(context!!).load(R.drawable.ic_logo_smartfren)
                        .override(100, 100)
                        .into(iv_logo_provider)
                    rv_mobile_credit.visibility = View.VISIBLE
                    prefixDetected = true
                    provider = "smartfren"
                }
                in listCodeAxis!! -> {
                    listCreditCall?.getListCreditAxis()
                        ?.enqueue(object : Callback<GetListCreditResponse> {
                            override fun onFailure(
                                call: Call<GetListCreditResponse>,
                                t: Throwable
                            ) {
                                pb_main.visibility = View.GONE
                                Toast.makeText(
                                    context,
                                    "System is not responding. Please try again.",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }

                            override fun onResponse(
                                call: Call<GetListCreditResponse>,
                                response: Response<GetListCreditResponse>
                            ) {
                                pb_main.visibility = View.GONE
                                if (response.isSuccessful) {
                                    setupGridRecycleView(response.body()?.amounts!!)
                                } else {
                                    Toast.makeText(
                                        context,
                                        "Provider unrecognized",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }

                        })
                    Glide.with(context!!).load(R.drawable.ic_logo_axis)
                        .override(100, 100)
                        .into(iv_logo_provider)
                    rv_mobile_credit.visibility = View.VISIBLE
                    prefixDetected = true
                    provider = "axis"
                }
                else -> {
                    et_phone_number_main.error = "Phone number unidentified."
                    iv_logo_provider.setImageResource(R.color.transparent)
                    rv_mobile_credit.visibility = View.GONE
                    prefixDetected = false
                }
            }
        }
    }

    private fun setupGridRecycleView(listData: List<Amount>) {
        recycleViewAdapter?.setDataList(listData)
        rv_mobile_credit.adapter = recycleViewAdapter

        recycleViewAdapter?.setOnItemCallback(object : MobileCreditRVAdapter.OnItemCallback {
            override fun onItemClicked(position: Int) {
                tv_amount_credit.text =
                    "Mobile Credit Rp${listData[position].amount}"
                tv_amount_pay.text = "Rp${listData[position].amount}"
            }
        })
    }

    private fun showInfoFragment(
        infoModel: InfoFragmentModel,
        transInProgressData: TransHistoryInProgressModel
    ) {
        val fragmentInfo = InfoFragment()
        val bundle = Bundle()
        val fragmentTransaction = activity?.supportFragmentManager?.beginTransaction()

        bundle.putParcelable(INFO_DATA, infoModel)
        bundle.putParcelable(TRANS_IN_PROGRESS, transInProgressData)

        fragmentInfo.arguments = bundle
        fragmentInfo.show(fragmentTransaction!!, "infofragment")
    }

    private fun processBottomNav() {
        btm_nav_credit.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.menu_history -> {
                    activity?.supportFragmentManager?.beginTransaction()
                        ?.replace(R.id.fl_container_main, TransactionHistoryFragment(), "history")
                        ?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        ?.addToBackStack(null)
                        ?.commit()
                    return@setOnNavigationItemSelectedListener true
                }
                R.id.menu_profile -> {
                    activity?.supportFragmentManager?.beginTransaction()
                        ?.replace(R.id.fl_container_main, ProfileFragment(), "profile")
                        ?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        ?.addToBackStack(null)
                        ?.commit()
                    return@setOnNavigationItemSelectedListener true
                }
                else -> false
            }

        }
    }

    override fun onResume() {
        super.onResume()
        val menuItem = btm_nav_credit.menu.getItem(0)
        menuItem.isChecked = true
        prefixDetected = false

        if (inputtedPhoneNumber != "") {
            et_phone_number_main.setText(inputtedPhoneNumber)
        }

        val transInProgressData = getTransInProgress()
        if (transInProgressData.phoneNumber != "" && transInProgressData.product != "" && transInProgressData.totalPayment != ""
            && transInProgressData.date != ""
        ) {
            transData = transInProgressData
            et_phone_number_main.setText(transInProgressData.phoneNumber.substring(3))
            tv_amount_credit.text = transInProgressData.product
            tv_amount_pay.text = transInProgressData.totalPayment

            bg_grey.visibility = View.VISIBLE
            bottomSheetBehavior?.state = BottomSheetBehavior.STATE_COLLAPSED

            if (et_phone_number_main.text.toString() != "") {
                et_phone_number_main.error = null
            }

            val prefs = context?.getSharedPreferences(TRANS_PENDING_DATA, MODE_PRIVATE)
            prefs?.edit()?.remove(DATA_LIST_TRANS)?.apply()
        }

        val prefs = context?.getSharedPreferences(
            TRANS_PENDING_DATA, MODE_PRIVATE
        )
        if (prefs?.getBoolean(IS_FROM_HISTORY, false) != null) {
            isFromHistory = prefs.getBoolean(IS_FROM_HISTORY, false)
        }

        updateBalance()
    }

    private fun updateBalance() {
        val sharedPreferences = context?.getSharedPreferences(AMOUNT_TRANS_SUCCESS, MODE_PRIVATE)
        val sharedPreferencesBalance = context?.getSharedPreferences(BUNDLE_DATA_USER, MODE_PRIVATE)
        val newBalance = sharedPreferencesBalance?.getString(BALANCE, "")?.toInt()?.minus(
            sharedPreferences!!.getInt(
                BALANCE, 0
            )
        )

        Log.d("TestingMain", newBalance.toString())
        Log.d("TestingMain", sharedPreferencesBalance?.getString(BALANCE, "")!!)
        Log.d("TestingMain", sharedPreferences!!.getInt(
            BALANCE, 0
        ).toString())

        setBalance(newBalance.toString())
        sharedPreferencesBalance?.edit()?.putString(BALANCE, newBalance.toString())?.apply()
        sharedPreferences?.edit()?.remove(BALANCE)?.apply()
    }

    private fun getTransInProgress(): TransHistoryInProgressModel {
        val prefs = context?.getSharedPreferences(
            TRANS_PENDING_DATA,
            MODE_PRIVATE
        )
        val gson = Gson()
        val jsonDataTrans = prefs?.getString(DATA_LIST_TRANS, null)
        return if (jsonDataTrans != null) {
            val type: Type = object : TypeToken<TransHistoryInProgressModel?>() {}.type
            gson.fromJson(jsonDataTrans, type)
        } else {
            TransHistoryInProgressModel(
                "",
                "",
                "",
                "",
                0
            )
        }
    }

    private fun cancelPreviousTransaction(userPhoneNumber: String) {
        if (userPhoneNumber != null) {
            APIClient().getClient()?.create(APIInterface::class.java)
                ?.postCancelTrans(PostCancelTrans(userPhoneNumber))
                ?.enqueue(object : Callback<PostCancelTransResModel> {
                    override fun onFailure(call: Call<PostCancelTransResModel>, t: Throwable) {
                        Toast.makeText(context, "Connection issue.", Toast.LENGTH_SHORT)
                            .show()
                    }

                    override fun onResponse(
                        call: Call<PostCancelTransResModel>,
                        response: Response<PostCancelTransResModel>
                    ) {
                        if (response.isSuccessful) {
                        } else {
                            val jsonObject: JSONObject?
                            try {
                                jsonObject = JSONObject(response.errorBody()!!.string())
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        }
                    }
                })
        }
    }

    private fun setBalance(balance: String) {
        when (balance.length) {
            4 -> tv_balance_credit.text =
                "Rp${balance.first()}.${balance.subSequence(0..balance.length-1)}"
            5 -> tv_balance_credit.text =
                "Rp${balance.subSequence(0, 2)}.${balance.subSequence(2..balance.length-1)}"
            6 -> tv_balance_credit.text =
                "Rp${balance.subSequence(0, 3)}.${balance.subSequence(3..balance.length-1)}"
            7 -> tv_balance_credit.text =
                "Rp${balance.first()}.${balance.subSequence(0..2)}." +
                        "${balance.subSequence(2 until balance.length-1)}"
        }
    }
}
