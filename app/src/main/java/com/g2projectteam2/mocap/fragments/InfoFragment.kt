package com.g2projectteam2.mocap.fragments

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentTransaction
import androidx.lifecycle.Observer
import com.g2projectteam2.mocap.LoginActivity
import com.g2projectteam2.mocap.MainActivity
import com.g2projectteam2.mocap.MainActivity.Companion.MAIN_FRAGMENT_NAME
import com.g2projectteam2.mocap.R
import com.g2projectteam2.mocap.database.entity.TransHistoryInProgressEntity
import com.g2projectteam2.mocap.fragments.CreditMainFragment.Companion.INFO_DATA
import com.g2projectteam2.mocap.fragments.CreditMainFragment.Companion.NEXT
import com.g2projectteam2.mocap.fragments.CreditMainFragment.Companion.OK
import com.g2projectteam2.mocap.fragments.CreditMainFragment.Companion.TRANS_IN_PROGRESS
import com.g2projectteam2.mocap.models.TransHistoryInProgressModel
import com.g2projectteam2.mockup.models.InfoFragmentModel
import kotlinx.android.synthetic.main.fragment_info.*

class InfoFragment : DialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setStyle(STYLE_NO_TITLE, R.style.CustomDialogFrag)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_info, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var transPending: TransHistoryInProgressModel? = null
        var infoModel: InfoFragmentModel? = null

        if (arguments != null) {
            infoModel = arguments?.getParcelable(INFO_DATA)
            tv_info_title.text = infoModel?.title
            tv_message_1.text = infoModel?.message1
            tv_message_2.text = infoModel?.message2
            tv_message_3.text = infoModel?.message3
            tv_message_4.text = infoModel?.message4
            btn_fragment_info.text = infoModel?.buttonMessage

            transPending = arguments?.getParcelable(TRANS_IN_PROGRESS)
        }

        btn_fragment_info.setOnClickListener {
            when (btn_fragment_info.text) {
                OK -> {
                    val totalBackstack = activity?.supportFragmentManager?.backStackEntryCount
                    if (totalBackstack == 1) {
                        dismiss()
                    } else {
                        activity?.supportFragmentManager?.popBackStack(MAIN_FRAGMENT_NAME, 0)
                        dismiss()
                    }
                    //TODO ilangin data dari pending dan masukin history ke retrofit
                    val historyViewModel = MainActivity().getHistoryMView()
                    if (transPending != null) {
                        val transData = TransHistoryInProgressEntity(
                            transPending.phoneNumber,
                            transPending.date,
                            transPending.product,
                            transPending.totalPayment
                        )
                        val id = transPending.id
                        transData.id = id
                        historyViewModel.delete(transData)
                    }


                }
                NEXT -> {
                    val transferReceiptPreviewFragment = TransferReceiptPreviewFragment()
                    val bundle = Bundle()
                    if (transPending != null){
                        bundle.putParcelable(TRANS_IN_PROGRESS, transPending)
                    }
                    transferReceiptPreviewFragment.arguments = bundle
                    activity?.supportFragmentManager?.beginTransaction()
                        ?.replace(
                            R.id.fl_container_main,
                            transferReceiptPreviewFragment,
                            "transferreceiptpreview"
                        )
                        ?.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        ?.addToBackStack(null)
                        ?.commit()
                    dismiss()
                }
            }
        }
    }

}
