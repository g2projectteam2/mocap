package com.g2projectteam2.mocap.database.entity

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Entity(tableName = "trans_history_in_progress_table")
@Parcelize
data class TransHistoryInProgressEntity (
    val phoneNumber: String,
    val date: String,
    val product: String,
    val totalPayment: String
): Parcelable {
    @PrimaryKey(autoGenerate = true)
    var id: Int = 0
}