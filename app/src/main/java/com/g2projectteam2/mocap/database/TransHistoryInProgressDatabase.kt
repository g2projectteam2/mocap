package com.g2projectteam2.mocap.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteDatabase
import com.g2projectteam2.mocap.database.dao.TransHistoryInProgressDao
import com.g2projectteam2.mocap.database.entity.TransHistoryInProgressEntity

@Database(entities = [TransHistoryInProgressEntity::class], version = 1)
abstract class TransHistoryInProgressDatabase: RoomDatabase() {
    abstract fun transHistoryDao(): TransHistoryInProgressDao
    companion object {
        private var instance: TransHistoryInProgressDatabase? = null

        fun getInstance(context: Context): TransHistoryInProgressDatabase {
            if (instance == null){
                synchronized(TransHistoryInProgressDatabase::class){
                    instance = Room.databaseBuilder(context.applicationContext, TransHistoryInProgressDatabase::class.java, "trans_history_in_progress_database")
                        .fallbackToDestructiveMigration()
                        .addCallback(roomCallback)
                        .build()
                }
            }
            return instance!!
        }

        fun destroyInstance(){
            instance = null
        }

        private val roomCallback = object: RoomDatabase.Callback(){
            override fun onCreate(db: SupportSQLiteDatabase) {
                super.onCreate(db)
            }
        }
    }
}