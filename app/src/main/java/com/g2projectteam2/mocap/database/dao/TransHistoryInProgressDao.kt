package com.g2projectteam2.mocap.database.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import com.g2projectteam2.mocap.database.entity.TransHistoryInProgressEntity

@Dao
interface TransHistoryInProgressDao {
    @Insert
    fun insert(news: TransHistoryInProgressEntity)

    @Query("DELETE FROM trans_history_in_progress_table")
    fun deleteAllHistory()

    @Delete
    fun delete(transHistoryInProgressEntity: TransHistoryInProgressEntity)

    @Query("SELECT * FROM trans_history_in_progress_table")
    fun getAllTransHistory(): LiveData<List<TransHistoryInProgressEntity>>
}