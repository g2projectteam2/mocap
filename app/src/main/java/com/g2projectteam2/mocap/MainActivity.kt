package com.g2projectteam2.mocap

import android.content.Context
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.g2projectteam2.mocap.LoginActivity.Companion.BALANCE
import com.g2projectteam2.mocap.LoginActivity.Companion.EMAIL
import com.g2projectteam2.mocap.LoginActivity.Companion.ID
import com.g2projectteam2.mocap.LoginActivity.Companion.NAME
import com.g2projectteam2.mocap.LoginActivity.Companion.PHONE
import com.g2projectteam2.mocap.adapters.TransHistoryInProgressRVAdapter
import com.g2projectteam2.mocap.api.APIClient
import com.g2projectteam2.mocap.api.APIInterface
import com.g2projectteam2.mocap.database.entity.TransHistoryInProgressEntity
import com.g2projectteam2.mocap.di.dbModule
import com.g2projectteam2.mocap.di.repositoryModule
import com.g2projectteam2.mocap.di.uiModule
import com.g2projectteam2.mocap.fragments.CreditMainFragment
import com.g2projectteam2.mocap.models.*
import com.g2projectteam2.mocap.viewmodel.TransHistoryViewModel
import com.google.gson.Gson
import org.json.JSONException
import org.json.JSONObject
import org.koin.android.ext.android.inject
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import org.koin.core.context.stopKoin
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    //TODO add progress bar every retrofit call

    private val transHistoryViewModel: TransHistoryViewModel by inject()
    private val adapter: TransHistoryInProgressRVAdapter by inject()

    companion object {
        const val MAIN_FRAGMENT_NAME = "mainfragment"
        const val BUNDLE_DATA_USER = "bundledatauser"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val creditMainFragment = CreditMainFragment()
        supportFragmentManager.beginTransaction()
            .replace(R.id.fl_container_main, creditMainFragment, "creditmain")
            .addToBackStack(MAIN_FRAGMENT_NAME)
            .commit()

        if (intent.extras != null) {
            val sharedPreferences =
                getSharedPreferences(BUNDLE_DATA_USER, Context.MODE_PRIVATE).edit()
            sharedPreferences.putString(PHONE, intent.getStringExtra(PHONE))
            sharedPreferences.putString(NAME, intent.getStringExtra(NAME))
            sharedPreferences.putString(EMAIL, intent.getStringExtra(EMAIL))
            sharedPreferences.putString(ID, intent.getStringExtra(ID))
            sharedPreferences.putString(BALANCE, intent.getStringExtra(BALANCE))
                .apply()
        }

        startKoin {
            androidContext(this@MainActivity)
            modules(
                listOf(
                    dbModule,
                    repositoryModule,
                    uiModule
                )
            )
        }
    }

    fun getAdapterTransHistory(): TransHistoryInProgressRVAdapter {
        return adapter
    }

    fun getHistoryMView(): TransHistoryViewModel {
        return transHistoryViewModel
    }

    override fun onBackPressed() {
        if (supportFragmentManager.backStackEntryCount == 1) {
            finish()
        } else {
            val currentFragment = supportFragmentManager.findFragmentByTag("creditmain")
            if (currentFragment != null && currentFragment.isVisible) {
                finish()
            }
            supportFragmentManager.popBackStack()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        stopKoin()
    }
}
