package com.g2projectteam2.mocap

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.view.animation.AnimationUtils
import com.g2projectteam2.mocap.LoginActivity.Companion.PREFERENCE
import kotlinx.android.synthetic.main.activity_splash.*

class SplashActivity : AppCompatActivity() {

    override fun onDestroy() {
        super.onDestroy()
        overridePendingTransition(R.anim.fade_in,R.anim.fade_out)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        iv_Splash.startAnimation(AnimationUtils.loadAnimation(this, R.anim.splash_in))
        Handler().postDelayed({
            iv_Splash.startAnimation(AnimationUtils.loadAnimation(this,R.anim.splash_out))
            Handler().postDelayed({
                iv_Splash.visibility= View.GONE
                val sharedPreferences = getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)
                val loginStatus = sharedPreferences.getBoolean(PREFERENCE,false)
                if (loginStatus){
                    startActivity(Intent(this, MainActivity::class.java))
                }else{
                    startActivity(Intent(this, LoginActivity::class.java))
                }

                finish()
            },500)
        },3000)

        tv_Splash.startAnimation(AnimationUtils.loadAnimation(this, R.anim.splash_in))
        Handler().postDelayed({
            tv_Splash.startAnimation(AnimationUtils.loadAnimation(this,R.anim.splash_out))
            Handler().postDelayed({
                tv_Splash.visibility= View.GONE
            },500)
        },3000)

        tv_Splash_version.startAnimation(AnimationUtils.loadAnimation(this, R.anim.splash_in))
        Handler().postDelayed({
            tv_Splash_version.startAnimation(AnimationUtils.loadAnimation(this,R.anim.splash_out))
            Handler().postDelayed({
                tv_Splash_version.visibility= View.GONE
            },500)
        },3000)




    }
}
