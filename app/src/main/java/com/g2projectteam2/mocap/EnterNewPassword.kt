package com.g2projectteam2.mocap

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import com.g2projectteam2.mocap.NewPasswordVerification.Companion.IDNEWPASS
import com.g2projectteam2.mocap.api.APIClient
import com.g2projectteam2.mocap.api.APIInterface
import com.g2projectteam2.mocap.model.PostRegisterResponse
import com.g2projectteam2.mocap.model.PostUpdatePassword
import kotlinx.android.synthetic.main.activity_enter_new_password.*
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_register.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.regex.Matcher
import java.util.regex.Pattern

class EnterNewPassword : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_enter_new_password)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btn_new_password_reset.setOnClickListener {
            if (et_new_password_password1.text.toString().isEmpty() || et_new_password_password2.text.toString().isEmpty()) {
                Toast.makeText(this, "Field cannot be empty", Toast.LENGTH_SHORT).show()

            } else if(et_new_password_password1.text.toString()!=et_new_password_password2.text.toString()){
                Toast.makeText(this, "Both field must be equal.", Toast.LENGTH_SHORT).show()
            }
            else {

                val idUserNewPass = intent.getIntExtra(IDNEWPASS, 0)

                val updatePasswordUser =
                    APIClient().getClient()?.create(APIInterface::class.java)
                        ?.postUpdatePasswordUser(
                            PostUpdatePassword(
                                idUserNewPass,
                                et_new_password_password2.text.toString()
                            )
                        )
                updatePasswordUser?.enqueue(object: Callback<PostRegisterResponse>{
                    override fun onFailure(call: Call<PostRegisterResponse>, t: Throwable) {
                        Toast.makeText(
                            applicationContext,
                            "Connection Error",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    override fun onResponse(
                        call: Call<PostRegisterResponse>,
                        response: Response<PostRegisterResponse>
                    ) {
                        if (response.isSuccessful){
                            val intent = Intent(applicationContext, NewPasswordInfo::class.java)
                            startActivity(intent)
                            finish()
                        }
                        else{
                            Toast.makeText(
                                applicationContext,
                                "Something is wrong, Please try again",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }
                })


            }


        }

        et_new_password_password1.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().isEmpty()) {
                    et_new_password_password1.setError("field cannot be empty")
                    btn_new_password_reset.isEnabled = false
                } else if (s.toString().length < 8) {
                    et_new_password_password1.setError("At least have 8 digits")
                    btn_new_password_reset.isEnabled = false
                } else if (isValidPassword(s.toString().trim())) {
                    et_new_password_password1.setError(null)
                    btn_new_password_reset.isEnabled = true
                } else {
                    et_new_password_password1.setError("At least have 1 capital letter, number and special character")
                    btn_new_password_reset.isEnabled = false
                }
            }

        })


        et_new_password_password2.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s?.toString().equals(et_new_password_password1?.text.toString())) {
                    et_new_password_password2.setError(null)
                    btn_new_password_reset.isEnabled = true
                } else if (s.toString().isEmpty()) {
                    et_new_password_password2.setError("Field cannot be blank")
                    btn_new_password_reset.isEnabled = false
                } else {
                    et_new_password_password2.setError("Must be equal to password")
                    btn_new_password_reset.isEnabled = false
                }
            }

        })
    }

    fun isValidPassword(password: String?): Boolean {
        val pattern: Pattern
        val matcher: Matcher
        val PASSWORD_PATTERN =
            "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=])(?=\\S+$).{4,}$"
        pattern = Pattern.compile(PASSWORD_PATTERN)
        matcher = pattern.matcher(password)
        return matcher.matches()
    }
}
