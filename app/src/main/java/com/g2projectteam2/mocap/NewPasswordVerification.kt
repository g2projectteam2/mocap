package com.g2projectteam2.mocap

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.widget.Toast
import com.g2projectteam2.mocap.ForgotPassword.Companion.IDOTP
import com.g2projectteam2.mocap.api.APIClient
import com.g2projectteam2.mocap.api.APIInterface
import com.g2projectteam2.mocap.model.PostForgotPasswordValidate
import com.g2projectteam2.mocap.model.PostRegisterResponse
import com.g2projectteam2.mocap.model.PostResendOTP
import com.g2projectteam2.mocap.model.ResponseResendOTP
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_new_password_verification.*
import kotlinx.android.synthetic.main.activity_register_verification.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class NewPasswordVerification : AppCompatActivity() {

    companion object {
        const val IDNEWPASS = "id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_password_verification)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btn_verify_new_password.setOnClickListener {

            if (et_otp_new_verification.text.toString().isEmpty()) {
                Toast.makeText(this, "Field cannot be empty", Toast.LENGTH_SHORT).show()
            } else {


                val idUserOTP = intent.getIntExtra(IDOTP, 0)

                val forgotPasswordValidateUser =
                    APIClient().getClient()?.create(APIInterface::class.java)
                        ?.postForgotPasswordValidateUser(
                            PostForgotPasswordValidate(
                                idUserOTP,
                                et_otp_new_verification.text.toString()
                            )
                        )
                forgotPasswordValidateUser?.enqueue(object : Callback<PostRegisterResponse> {
                    override fun onFailure(call: Call<PostRegisterResponse>, t: Throwable) {
                        Toast.makeText(
                            applicationContext,
                            "Connection Error",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                    override fun onResponse(
                        call: Call<PostRegisterResponse>,
                        response: Response<PostRegisterResponse>
                    ) {
                        if (response.isSuccessful) {
                            val intent = Intent(applicationContext, EnterNewPassword::class.java)
                            intent.putExtra(IDNEWPASS, response.body()?.user!!.id)
                            startActivity(intent)
                            finish()
                        } else {
                            Toast.makeText(
                                applicationContext,
                                "Wrong OTP code",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                    }

                })

            }
        }

        et_otp_new_verification.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s.toString().isEmpty()) {
                    et_otp_new_verification.setError("field cannot be empty")
                    btn_verify_new_password.isEnabled = false
                } else if (s.toString().length < 6 || s.toString().length > 6) {
                    et_otp_new_verification.setError("OTP code must be 6 digits")
                    btn_verify_new_password.isEnabled = false
                } else {
                    et_otp_new_verification.setError(null)
                    btn_verify_new_password.isEnabled = true
                }
            }

        })
        object : CountDownTimer(30000, 1000) {

            override fun onTick(millisUntilFinished: Long) {

                btn_timer_new_password.isEnabled = false
                var diff = millisUntilFinished
                val secondsInMilli: Long = 1000
                val minutesInMilli = secondsInMilli * 60

                val elapsedMinutes = diff / minutesInMilli
                diff %= minutesInMilli
                val elapsedSeconds = diff / secondsInMilli
                tv_timer_otp_new_password.text =
                    "Time remaining : $elapsedMinutes min $elapsedSeconds sec"
            }

            override fun onFinish() {
                tv_timer_otp_new_password.text =
                    "                            Time is up! \n Click button below to send another code"
                btn_timer_new_password.isEnabled = true
                btn_verify_new_password.isEnabled = false

            }
        }.start()

        Toast.makeText(this, "OTP Verification code has been sent to you", Toast.LENGTH_SHORT)
            .show()

        btn_timer_new_password.setOnClickListener {


            val idUserOTP = intent.getIntExtra(IDOTP, 0)

            val resendOTPUser = APIClient().getClient()?.create(APIInterface::class.java)
                ?.postResendOTPUser(
                    PostResendOTP(idUserOTP)
                )
            resendOTPUser?.enqueue(object : Callback<ResponseResendOTP>{
                override fun onFailure(call: Call<ResponseResendOTP>, t: Throwable) {
                    Toast.makeText(applicationContext,"Connection error",Toast.LENGTH_SHORT).show()
                }

                override fun onResponse(
                    call: Call<ResponseResendOTP>,
                    response: Response<ResponseResendOTP>
                ) {
                    if(response.isSuccessful){
                        Toast.makeText(applicationContext,"New code OTP has been sent",Toast.LENGTH_SHORT).show()
                    }else{
                        Toast.makeText(applicationContext,"Error request",Toast.LENGTH_SHORT).show()
                    }
                }
            })

            object : CountDownTimer(30000, 1000) {


                override fun onTick(millisUntilFinished: Long) {


                    btn_verify_new_password.isEnabled = true
                    btn_timer_new_password.isEnabled = false
                    var diff = millisUntilFinished
                    val secondsInMilli: Long = 1000
                    val minutesInMilli = secondsInMilli * 60

                    val elapsedMinutes = diff / minutesInMilli
                    diff %= minutesInMilli
                    val elapsedSeconds = diff / secondsInMilli
                    tv_timer_otp_new_password.text =
                        "Time remaining : $elapsedMinutes min $elapsedSeconds sec"
                }

                override fun onFinish() {
                    tv_timer_otp_new_password.text =
                        "                            Time is up! \n Click button below to send another code"

                    btn_verify_new_password.isEnabled = false
                    btn_timer_new_password.isEnabled = true
                }
            }.start()

            Toast.makeText(
                this,
                " New OTP verification code has been sent to you",
                Toast.LENGTH_SHORT
            )
                .show()
        }
    }

}
