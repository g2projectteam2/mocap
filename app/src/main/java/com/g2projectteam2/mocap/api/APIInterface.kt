package com.g2projectteam2.mocap.api

import com.g2projectteam2.mocap.model.*
import com.g2projectteam2.mocap.models.*
import retrofit2.Call
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface APIInterface {
    @POST("register/validate")
    fun postRegisterUser(@Body postRegister: PostRegister): Call<PostRegisterResponse>
    @POST("login")
    fun postLoginEmailUser(@Body postLogin: PostLoginEmail): Call<PostRegisterResponse>
    @POST("login")
    fun postLoginPhoneUser(@Body postLogin: PostLoginPhone): Call<PostRegisterResponse>
    @POST("register/activate")
    fun postRegisterActivateUser(@Body postRegisterActivate: PostRegisterActivateUser): Call<PostRegisterResponse>
    @POST("register/activate")
    fun postRegisterActivateUserId(@Body postRegisterActivateId: PostRegisterActivateUserId): Call<PostRegisterResponse>

    @GET("mobilecredit/amount?provider=xl")
    fun getListCreditXL(): Call<GetListCreditResponse>
    @GET("mobilecredit/amount?provider=telkomsel")
    fun getListCreditTelkomsel(): Call<GetListCreditResponse>
    @GET("mobilecredit/amount?provider=tri")
    fun getListCreditTri(): Call<GetListCreditResponse>
    @GET("mobilecredit/amount?provider=indosat")
    fun getListCreditIndosat(): Call<GetListCreditResponse>
    @GET("mobilecredit/amount?provider=smartfren")
    fun getListCreditSmartfren(): Call<GetListCreditResponse>
    @GET("mobilecredit/amount?provider=axis")
    fun getListCreditAxis(): Call<GetListCreditResponse>

    @POST("mobilecredit/balance")
    fun postTransInApp(@Body postTransModel: PostTransModel): Call<PostTransInAppResModel>
    @POST("mobilecredit/banktransfer")
    fun postTransBank(@Body postTransModel: PostTransModel): Call<PostTransBankResModel>
    @POST("mobilecredit/canceltransfer")
    fun postCancelTrans(@Body postCancelTrans: PostCancelTrans): Call<PostCancelTransResModel>

    @POST("mobilecredit/validatetransfer")
    fun postValidateTrans(@Body postValidateTrans: PostValidateTrans): Call<PostValidateTransResModel>

    @GET("mobilecredit/history")
    fun getTransHistory(@Query("userid") userid:Int): Call<GetTransHistoryResModel>

    @POST("forgotPassword/request")
    fun postForgotEmailUser(@Body postForgotEmail: PostForgotEmail): Call<PostRegisterResponse>
    @POST("forgotPassword/request")
    fun postForgotPhoneUser(@Body postForgotPhone: PostForgotPhone): Call<PostRegisterResponse>
    @POST("forgotPassword/validate")
    fun postForgotPasswordValidateUser(@Body postForgotPasswordValidate: PostForgotPasswordValidate): Call<PostRegisterResponse>
    @POST("updatePassword")
    fun postUpdatePasswordUser(@Body postUpdatePassword: PostUpdatePassword ): Call<PostRegisterResponse>
    @POST("resendOTP")
    fun postResendOTPUser(@Body postResendOTP: PostResendOTP): Call<ResponseResendOTP>

}