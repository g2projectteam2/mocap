package com.g2projectteam2.mocap

import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.g2projectteam2.mocap.LoginVerificationActiviry.Companion.IDREGLOGIN
import com.g2projectteam2.mocap.RegisterActivity.Companion.EMAILREG
import com.g2projectteam2.mocap.RegisterActivity.Companion.IDREG
import com.g2projectteam2.mocap.RegisterActivity.Companion.OTPREG
import com.g2projectteam2.mocap.api.APIClient
import com.g2projectteam2.mocap.api.APIInterface
import com.g2projectteam2.mocap.model.*
import kotlinx.android.synthetic.main.activity_register_verification.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class RegisterVerification : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register_verification)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        btn_verify_register.setOnClickListener {
            if (et_otp_register_verification.text.toString().isEmpty()) {
                Toast.makeText(this, "Field cannot be empty", Toast.LENGTH_SHORT).show()
            } else {

                if(intent.getStringExtra(EMAILREG)!=null){
                    val registerVerificationUser =
                        APIClient().getClient()?.create(APIInterface::class.java)
                            ?.postRegisterActivateUser(
                                PostRegisterActivateUser(
                                    intent.getStringExtra(EMAILREG),
                                    et_otp_register_verification.text.toString()
                                )
                            )
                    registerVerificationUser?.enqueue(object : Callback<PostRegisterResponse> {
                        override fun onFailure(call: Call<PostRegisterResponse>, t: Throwable) {
                            Toast.makeText(
                                applicationContext,
                                "Connection Error",
                                Toast.LENGTH_SHORT
                            ).show()
                            Log.d("Failure",t.stackTrace.toString())
                            Log.d("Failure",t.message.toString())
                        }

                        override fun onResponse(
                            call: Call<PostRegisterResponse>,
                            response: Response<PostRegisterResponse>
                        ) {
                            if (response.isSuccessful) {
                                Toast.makeText(
                                    applicationContext,
                                    response.body()?.status,
                                    Toast.LENGTH_SHORT
                                ).show()
                                Log.d("Hasil", response.body()?.toString()!!)

                                Log.d("OTP" , response.body().toString())

                                if(response.body()!!.user?.activeStatus!!){

                                    val intent = Intent(applicationContext, RegisterInfo::class.java)
                                    startActivity(intent)
                                    finish()
                                }else{
                                    Toast.makeText(
                                        applicationContext,
                                        "Please input the correct OTP code",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                            else {
                                Toast.makeText(
                                    applicationContext,
                                    "Please input the correct OTP code",
                                    Toast.LENGTH_SHORT
                                ).show()
                                if(response.body()!=null){
                                    Log.d("Testing", response.body()?.toString()!!)
                                }else{
                                    Log.d("Testing", "body null")
                                }

                            }
                        }
                    })
                }else{
                    val registerVerificationUserId =
                        APIClient().getClient()?.create(APIInterface::class.java)
                            ?.postRegisterActivateUserId(
                                PostRegisterActivateUserId(
                                    intent.getIntExtra(IDREGLOGIN,0),
                                    et_otp_register_verification.text.toString()
                                )
                            )
                    registerVerificationUserId?.enqueue(object : Callback<PostRegisterResponse> {
                        override fun onFailure(call: Call<PostRegisterResponse>, t: Throwable) {
                            Toast.makeText(
                                applicationContext,
                                "Connection Error",
                                Toast.LENGTH_SHORT
                            ).show()
                            Log.d("Failure",t.stackTrace.toString())
                            Log.d("Failure",t.message.toString())
                        }

                        override fun onResponse(
                            call: Call<PostRegisterResponse>,
                            response: Response<PostRegisterResponse>
                        ) {
                            if (response.isSuccessful) {
                                Toast.makeText(
                                    applicationContext,
                                    response.body()?.status,
                                    Toast.LENGTH_SHORT
                                ).show()
                                Log.d("Hasil", response.body()?.toString()!!)

                                Log.d("OTP" , response.body().toString())

                                if(response.body()!!.user?.activeStatus!!){

                                    val intent = Intent(applicationContext, RegisterInfo::class.java)
                                    startActivity(intent)
                                    finish()
                                }else{
                                    Toast.makeText(
                                        applicationContext,
                                        "Please input the correct OTP code",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            }
                            else {
                                Toast.makeText(
                                    applicationContext,
                                    "Please input the correct OTP code",
                                    Toast.LENGTH_SHORT
                                ).show()
                                if(response.body()!=null){
                                    Log.d("Testing", response.body()?.toString()!!)
                                }else{
                                    Log.d("Testing", "body null")
                                }

                            }
                        }
                    })
                }

            }
        }

        et_otp_register_verification.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s.toString().isEmpty()) {
                    et_otp_register_verification.setError("field cannot be empty")
                    btn_verify_register.isEnabled = false
                } else if (s.toString().length < 6 || s.toString().length > 6) {
                    et_otp_register_verification.setError("OTP code must be 6 digits")
                    btn_verify_register.isEnabled = false
                } else {
                    et_otp_register_verification.setError(null)
                    btn_verify_register.isEnabled = true
                }
            }

        })




        object : CountDownTimer(30000, 1000) {

            override fun onTick(millisUntilFinished: Long) {

                btn_timer_register.isEnabled = false
                var diff = millisUntilFinished
                val secondsInMilli: Long = 1000
                val minutesInMilli = secondsInMilli * 60

                val elapsedMinutes = diff / minutesInMilli
                diff %= minutesInMilli
                val elapsedSeconds = diff / secondsInMilli
                tv_timer_otp_register.text =
                    "Time remaining : $elapsedMinutes min $elapsedSeconds sec"
            }

            override fun onFinish() {
                tv_timer_otp_register.text =
                    "                            Time is up! \n Click button below to send another code"
                btn_timer_register.isEnabled = true
                btn_verify_register.isEnabled = false
            }
        }.start()

        Toast.makeText(this, "OTP Verification code has been sent to you", Toast.LENGTH_SHORT)
            .show()


        btn_timer_register.setOnClickListener {

            val otpOptions = intent.getBooleanExtra(OTPREG, false)
            val idUserOTP = intent.getIntExtra(IDREG, 0)
            val idUserOTPLogin =intent.getIntExtra(IDREGLOGIN,0)

            if(otpOptions){
                val resendOTPUser = APIClient().getClient()?.create(APIInterface::class.java)
                    ?.postResendOTPUser(
                        PostResendOTP(idUserOTP)
                    )
                resendOTPUser?.enqueue(object : Callback<ResponseResendOTP>{
                    override fun onFailure(call: Call<ResponseResendOTP>, t: Throwable) {
                        Toast.makeText(applicationContext,"Connection error",Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(
                        call: Call<ResponseResendOTP>,
                        response: Response<ResponseResendOTP>
                    ) {
                        if(response.isSuccessful){
                            Toast.makeText(applicationContext,"New code OTP has been sent",Toast.LENGTH_SHORT).show()
                        }else{
                            Toast.makeText(applicationContext,"Error request",Toast.LENGTH_SHORT).show()
                        }
                    }
                })

            }else{
                val resendOTPUser = APIClient().getClient()?.create(APIInterface::class.java)
                    ?.postResendOTPUser(
                        PostResendOTP(idUserOTPLogin)
                    )
                resendOTPUser?.enqueue(object : Callback<ResponseResendOTP>{
                    override fun onFailure(call: Call<ResponseResendOTP>, t: Throwable) {
                        Toast.makeText(applicationContext,"Connection error",Toast.LENGTH_SHORT).show()
                    }

                    override fun onResponse(
                        call: Call<ResponseResendOTP>,
                        response: Response<ResponseResendOTP>
                    ) {
                        if(response.isSuccessful){
                            Toast.makeText(applicationContext,"New code OTP has been sent",Toast.LENGTH_SHORT).show()
                        }else{
                            Toast.makeText(applicationContext,"Error request",Toast.LENGTH_SHORT).show()
                        }
                    }
                })

            }


            object : CountDownTimer(30000, 1000) {

                override fun onTick(millisUntilFinished: Long) {

                    btn_timer_register.isEnabled = false
                    btn_verify_register.isEnabled = true
                    var diff = millisUntilFinished
                    val secondsInMilli: Long = 1000
                    val minutesInMilli = secondsInMilli * 60

                    val elapsedMinutes = diff / minutesInMilli
                    diff %= minutesInMilli
                    val elapsedSeconds = diff / secondsInMilli
                    tv_timer_otp_register.text =
                        "Time remaining : $elapsedMinutes min $elapsedSeconds sec"
                }

                override fun onFinish() {
                    tv_timer_otp_register.text =
                        "                            Time is up! \n Click button below to send another code"
                    btn_timer_register.isEnabled = true
                    btn_verify_register.isEnabled = false
                }
            }.start()

            Toast.makeText(this, "OTP Verification code has been sent to you", Toast.LENGTH_SHORT)
                .show()
        }
    }


}
