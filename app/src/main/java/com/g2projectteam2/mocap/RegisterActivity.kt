package com.g2projectteam2.mocap

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.g2projectteam2.mocap.api.APIClient
import com.g2projectteam2.mocap.api.APIInterface
import com.g2projectteam2.mocap.model.PostRegister
import com.g2projectteam2.mocap.model.PostRegisterResponse
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.regex.Matcher
import java.util.regex.Pattern


class RegisterActivity : AppCompatActivity() {

    companion object {
        const val EMAILREG = "email"
        const val OTPREG = "otp"
        const val IDREG = "id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        et_password_register.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().isEmpty()) {
                    et_password_register.setError("field cannot be empty")
                } else if (s.toString().length < 8) {
                    et_password_register.setError("At least have 8 digits")
                } else if (isValidPassword(s.toString().trim())) {
                    et_password_register.setError(null)
                } else {
                    et_password_register.setError("At least have 1 capital letter, number and special character")
                }
            }

        })

        et_password2_register.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {

                if (s?.toString().equals(et_password_register?.text.toString())) {
                    et_password2_register.setError(null)
                } else if (s.toString().isEmpty()) {
                    et_password2_register.setError("Field cannot be blank")
                } else {
                    et_password2_register.setError("Must be equal to password")
                }
            }

        })



        et_email_register.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().isEmpty()) {
                    et_email_register.setError("field cannot be empty")
                } else if (isEmailValid(s.toString().trim())) {
                    et_email_register.setError(null)
                } else {
                    et_email_register.setError("Invalid email")
                }
            }

        })
        et_name_register.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().isEmpty()) {
                    et_name_register.setError("field cannot be empty")
                } else {
                    et_name_register.setError(null)
                }

            }

        })

        et_phone_number_register.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s!!.isNotEmpty()) {
                    (s.first().toString() == "+" && s.length >= 12 && s.length <= 15)
                    val phoneNumberLength = (("+62$s").length in 12..15)
                    if (phoneNumberLength) {
                        providerCodeDetection(s)
                    } else {
                        et_phone_number_register.error = "Phone number unidentified."
                    }
                } else {
                    et_phone_number_register.error = "Field cannot be empty."
                }
            }

        })

        btn_register.setOnClickListener {

            if (et_password_register.text.toString().isEmpty() || et_password2_register.text.toString().isEmpty() || et_phone_number_register.text.toString().isEmpty() || et_email_register.text.toString().isEmpty() || et_name_register.text.toString().isEmpty()) {
                Toast.makeText(this, "Field Cannot Be Empty", Toast.LENGTH_SHORT).show()

            }
            else if(et_password_register.text.toString()!=et_password2_register.text.toString()){
                Toast.makeText(this, "Both password must be equal.", Toast.LENGTH_SHORT).show()
            }
            else {

                val registerUser = APIClient().getClient()?.create(APIInterface::class.java)
                    ?.postRegisterUser(
                        PostRegister(
                            et_email_register.text.toString(),
                            et_password2_register.text.toString(),
                            "+62${et_phone_number_register.text}",
                            et_name_register.text.toString()
                        )
                    )
                registerUser?.enqueue(object : Callback<PostRegisterResponse> {
                    override fun onFailure(call: Call<PostRegisterResponse>, t: Throwable) {
                        Toast.makeText(
                            applicationContext,
                            "Connection Error",
                            Toast.LENGTH_SHORT
                        ).show()
                        Log.d("Failure", t.stackTrace.toString())
                        Log.d("Failure", t.message.toString())
                    }

                    override fun onResponse(
                        call: Call<PostRegisterResponse>,
                        response: Response<PostRegisterResponse>
                    ) {

                        if (response.isSuccessful) {
                            Toast.makeText(
                                applicationContext,
                                response.body()?.status,
                                Toast.LENGTH_SHORT
                            ).show()
                            Log.d("Hasil Register", response.body()?.toString()!!)


                            val intent =
                                Intent(applicationContext, RegisterVerification::class.java)
                            intent.putExtra(EMAILREG, response.body()?.user!!.email.toString())
                            intent.putExtra(OTPREG, true)
                            intent.putExtra(IDREG, response.body()?.user!!.id)
                            startActivity(intent)
                            finish()

                        } else {

                            var jsonObject: JSONObject? = null
                            try {
                                jsonObject = JSONObject(response.errorBody()!!.string())
                                val statusMessage = jsonObject.getString("description")

                                if (statusMessage == "phone number already registered") {
                                    Toast.makeText(
                                        applicationContext,
                                        "Phone number has already used",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }else if(statusMessage == "email already registered"){
                                    Toast.makeText(
                                applicationContext,
                                    "Email has already used",
                                    Toast.LENGTH_SHORT
                                    ).show()
                                }else{
                                    Toast.makeText(
                                        applicationContext,
                                        "Email or phone number have already registered",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }
                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }



                        }

                    }
                })

            }
        }

    }

    fun isValidPassword(password: String?): Boolean {
        val pattern: Pattern
        val matcher: Matcher
        val PASSWORD_PATTERN =
            "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=.*[!@#$%^&+=])(?=\\S+$).{4,}$"
        pattern = Pattern.compile(PASSWORD_PATTERN)
        matcher = pattern.matcher(password)
        return matcher.matches()
    }

    fun isEmailValid(email: String): Boolean {
        val regExpn = ("^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                + "((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                + "([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                + "[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                + "([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$")
        val inputStr: CharSequence = email
        val pattern =
            Pattern.compile(regExpn, Pattern.CASE_INSENSITIVE)
        val matcher = pattern.matcher(inputStr)
        return if (matcher.matches()) true else false


    }

    fun providerCodeDetection(prefix: CharSequence) {
        val listCodeXL = this.resources?.getStringArray(R.array.code_xl)
        val listCodeTelkomsel = this.resources?.getStringArray(R.array.code_telkomsel)
        val listCodeTri = this.resources?.getStringArray(R.array.code_tri)
        val listCodeIndosat = this.resources?.getStringArray(R.array.code_indosat)
        val listCodeSmartfren = this.resources?.getStringArray(R.array.code_smartfren)
        val listCodeAxis = this.resources?.getStringArray(R.array.code_axis)

        when ("+62" + prefix.subSequence(0..2).toString()) {
            in listCodeXL!! -> {
            }
            in listCodeTelkomsel!! -> {
            }
            in listCodeTri!! -> {
            }
            in listCodeIndosat!! -> {
            }
            in listCodeSmartfren!! -> {
            }
            in listCodeAxis!! -> {
            }
            else -> {
                et_phone_number_register.setError("Provider Undetected")
            }
        }
    }
}

