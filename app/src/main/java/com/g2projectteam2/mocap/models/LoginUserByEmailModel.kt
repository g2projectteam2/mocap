package com.g2projectteam2.mocap.models

data class LoginUserByEmailModel (
    val email: String,
    val password: String
)