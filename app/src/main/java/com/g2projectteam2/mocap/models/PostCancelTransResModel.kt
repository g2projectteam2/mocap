package com.g2projectteam2.mocap.models

data class PostCancelTransResModel(
    val description: String,
    val status: String
)