package com.g2projectteam2.mocap.models

data class PostValidateTrans(
    val userPhone: String,
    val proof: String
)