package com.g2projectteam2.mocap.models

data class PostCancelTrans(
    val userPhone: String
)