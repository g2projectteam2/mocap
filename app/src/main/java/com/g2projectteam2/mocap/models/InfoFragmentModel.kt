package com.g2projectteam2.mockup.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class InfoFragmentModel(
    val title: String,
    val message1: String,
    val message2: String,
    val message3: String,
    val message4: String,
    val buttonMessage: String
): Parcelable