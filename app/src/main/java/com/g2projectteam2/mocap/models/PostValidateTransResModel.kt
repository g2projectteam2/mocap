package com.g2projectteam2.mocap.models

data class PostValidateTransResModel(
    val amount: Int,
    val description: String,
    val destinationPhone: String,
    val status: String,
    val transactionId: Int
)