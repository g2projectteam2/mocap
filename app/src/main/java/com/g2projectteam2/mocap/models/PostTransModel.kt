package com.g2projectteam2.mocap.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PostTransModel (
    val userPhone: String,
    val provider: String,
    val destinationPhone: String,
    val amount: Int
): Parcelable