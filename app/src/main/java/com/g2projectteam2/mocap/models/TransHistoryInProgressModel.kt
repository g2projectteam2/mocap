package com.g2projectteam2.mocap.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TransHistoryInProgressModel (
    val phoneNumber: String,
    val date: String,
    val product: String,
    val totalPayment: String,
    var id: Int
): Parcelable