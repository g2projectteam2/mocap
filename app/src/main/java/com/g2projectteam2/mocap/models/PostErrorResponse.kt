package com.g2projectteam2.mocap.models

data class PostErrorResponse (
    val description: String,
    val status: String
)