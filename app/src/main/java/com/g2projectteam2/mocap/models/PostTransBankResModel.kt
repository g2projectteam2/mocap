package com.g2projectteam2.mocap.models

data class PostTransBankResModel(
    val description: String,
    val status: String,
    val vaNumber: String
)