package com.g2projectteam2.mocap.models

data class PostRegisterModel (
    val email: String,
    val password: String,
    val phone: String,
    val name: String
)