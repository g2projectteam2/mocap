package com.g2projectteam2.mocap.models

data class GetTransHistoryResModel(
    val description: String,
    val status: String,
    val transactions: List<Transaction>
)

data class Transaction(
    val amount: Int,
    val description: String,
    val destinationPhone: String,
    val id: Int,
    val paymentType: String,
    val provider: String,
    val status: Boolean,
    val timestamp: String,
    val userid: Int
)