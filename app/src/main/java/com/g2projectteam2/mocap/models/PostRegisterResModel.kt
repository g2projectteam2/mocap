package com.g2projectteam2.mocap.models

data class PostRegisterResModel (
    val description: String,
    val user: UserModel,
    val status: String
)

data class UserModel (
    val password: String,
    val activeStatus: Boolean,
    val balance: Int,
    val phone: String,
    val name: String,
    val otp: String,
    val id: Int,
    val loginStatus: Boolean,
    val email: String
)