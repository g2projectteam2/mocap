package com.g2projectteam2.mocap.models

data class PostTransBankProofModel (
    val userPhone: String,
    val proof: String
)