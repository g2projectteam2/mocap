package com.g2projectteam2.mocap.models

data class GetListCreditResponse(
    val amounts: List<Amount>,
    val description: String,
    val status: String
)

data class Amount(
    val amount: Int
)