package com.g2projectteam2.mocap.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class TransHistoryCompletedModel (
    val phoneNumber: String,
    val date: String,
    val product: String,
    val totalPayment: String,
    val status: String
): Parcelable