package com.g2projectteam2.mocap.repository

import android.os.AsyncTask
import androidx.lifecycle.LiveData
import com.g2projectteam2.mocap.database.dao.TransHistoryInProgressDao
import com.g2projectteam2.mocap.database.entity.TransHistoryInProgressEntity

class DatabaseRepository(private val transHistoryInProgressDao: TransHistoryInProgressDao) {
    private val allData: LiveData<List<TransHistoryInProgressEntity>> = transHistoryInProgressDao.getAllTransHistory()

    private class InsertTransHistoryAsyncTask(val transHistoryInProgressDao: TransHistoryInProgressDao): AsyncTask<TransHistoryInProgressEntity, Unit, Unit>(){
        override fun doInBackground(vararg transHistoryInProgressEntity: TransHistoryInProgressEntity?) {
            transHistoryInProgressDao.insert(transHistoryInProgressEntity[0]!!)
        }
    }

    private class DeleteTransHistoryAsyncTask(val transHistoryInProgressDao: TransHistoryInProgressDao): AsyncTask<TransHistoryInProgressEntity, Unit, Unit>(){
        override fun doInBackground(vararg transHistoryInProgressEntity: TransHistoryInProgressEntity?) {
            transHistoryInProgressDao.delete(transHistoryInProgressEntity[0]!!)
        }
    }

    private class DeleteAllTransHistoryAsyncTask(val transHistoryInProgressDao: TransHistoryInProgressDao): AsyncTask<Unit, Unit, Unit>(){
        override fun doInBackground(vararg params: Unit?) {
            transHistoryInProgressDao.deleteAllHistory()
        }
    }

    fun insert(transHistoryInProgressEntity: TransHistoryInProgressEntity){
        InsertTransHistoryAsyncTask(transHistoryInProgressDao).execute(transHistoryInProgressEntity)
    }

    fun delete(transHistoryInProgressEntity: TransHistoryInProgressEntity){
        DeleteTransHistoryAsyncTask(transHistoryInProgressDao).execute(transHistoryInProgressEntity)
    }

    fun deleteAllHistory(){
        DeleteAllTransHistoryAsyncTask(transHistoryInProgressDao).execute()
    }

    fun getAllHistory(): LiveData<List<TransHistoryInProgressEntity>> {
        return allData
    }
}