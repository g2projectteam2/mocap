package com.g2projectteam2.mocap

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import com.g2projectteam2.mocap.api.APIClient
import com.g2projectteam2.mocap.api.APIInterface
import com.g2projectteam2.mocap.model.PostLoginEmail
import com.g2projectteam2.mocap.model.PostLoginPhone
import com.g2projectteam2.mocap.model.PostRegisterResponse
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_login.*
import kotlinx.android.synthetic.main.activity_register.*
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.regex.Matcher
import java.util.regex.Pattern


class LoginActivity : AppCompatActivity() {

    companion object {
        const val PREFERENCE = "PREFERENCE"
        const val BALANCE = "balance"
        const val PHONE = "phone"
        const val NAME = "name"
        const val EMAIL = "email"
        const val ID = "id"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)
        val sharedPreferences = getSharedPreferences("USER_INFO", Context.MODE_PRIVATE)

        et_email_login.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().isEmpty()) {
                    et_email_login.setError("field cannot be empty")
                    btn_login_login.isEnabled = false
                } else {
                    et_email_login.setError(null)
                    btn_login_login.isEnabled = true
                }
            }


        })

        et_password_login.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {}
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s.toString().isEmpty()) {
                    et_password_login.setError("field cannot be empty")
                    btn_login_login.isEnabled = false
                } else {
                    et_password_login.setError(null)
                    btn_login_login.isEnabled = true
                }
            }
        })

        btn_login_login.setOnClickListener {
            if (et_email_login.text.toString().isEmpty() || et_password_login.text.toString()
                    .isEmpty()
            ) {
                Toast.makeText(this, "Field cannot be empty.", Toast.LENGTH_SHORT).show()
            } else {

                if (et_email_login.text.toString().contains('@')) {
                    Log.d("tes", "if")
                    val loginEmailUser =
                        APIClient().getClient()?.create(APIInterface::class.java)
                            ?.postLoginEmailUser(
                                PostLoginEmail(
                                    et_email_login.text.toString(),
                                    et_password_login.text.toString()
                                )
                            )
                    loginEmailUser?.enqueue(object : Callback<PostRegisterResponse> {
                        override fun onFailure(call: Call<PostRegisterResponse>, t: Throwable) {
                            Toast.makeText(
                                applicationContext,
                                "Connection Error",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                        override fun onResponse(
                            call: Call<PostRegisterResponse>,
                            response: Response<PostRegisterResponse>
                        ) {
                            if (response.isSuccessful) {

                                if (response.body()?.user?.activeStatus!!) {

                                    Log.d("Hasil Login", response.body()?.toString()!!)
                                    val preferenceLogin = response.body()?.user?.loginStatus!!
                                    val editor = sharedPreferences.edit()
                                    editor.putBoolean(PREFERENCE, preferenceLogin)
                                    editor.apply()
                                    val intent =
                                        Intent(applicationContext, MainActivity::class.java)
                                    intent.putExtra(
                                        BALANCE,
                                        response.body()?.user!!.balance.toString()
                                    )
                                    intent.putExtra(PHONE, response.body()?.user!!.phone.toString())
                                    intent.putExtra(NAME, response.body()?.user!!.name.toString())
                                    intent.putExtra(EMAIL, response.body()?.user!!.email.toString())
                                    intent.putExtra(ID, response.body()?.user!!.id.toString())
                                    startActivity(intent)
                                    finish()
                                } else {
                                    Toast.makeText(
                                        applicationContext,
                                        "Acc has not been verified",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }

                            } else {
                                Toast.makeText(
                                    applicationContext,
                                    "Account does not exist or has not been verified",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }


                        }

                    })
                } else {

                    var inputNumber = et_email_login.text.toString()
                    var inputFix = ""

                    if (inputNumber.first() == '0') {
                        inputNumber = inputNumber.substring(1)
                        inputFix = "+62$inputNumber"
                        Log.d("Input2", inputFix)
                    } else if (inputNumber.first() == '6') {
                        inputFix = "+${inputNumber}"
                        Log.d("Input1", inputFix)
                    } else {
                        inputFix = inputNumber
                        Log.d("Input3", inputFix)
                    }
                    Log.d("tes", "else")
                    val loginPhoneUser =
                        APIClient().getClient()?.create(APIInterface::class.java)
                            ?.postLoginPhoneUser(
                                PostLoginPhone(
                                    inputFix,
                                    et_password_login.text.toString()
                                )
                            )
                    Log.d("input4", inputFix)
                    loginPhoneUser?.enqueue(object : Callback<PostRegisterResponse> {
                        override fun onFailure(call: Call<PostRegisterResponse>, t: Throwable) {
                            Toast.makeText(
                                applicationContext,
                                "Connection Error",
                                Toast.LENGTH_SHORT
                            ).show()
                        }

                        override fun onResponse(
                            call: Call<PostRegisterResponse>,
                            response: Response<PostRegisterResponse>
                        ) {
                            if (response.isSuccessful) {
                                Toast.makeText(
                                    applicationContext,
                                    response.body()?.status,
                                    Toast.LENGTH_SHORT
                                ).show()

                                if (response.body()?.user?.activeStatus!!) {

                                    val preferenceLogin = response.body()?.user?.loginStatus!!
                                    val editor = sharedPreferences.edit()
                                    editor.putBoolean(PREFERENCE, preferenceLogin)
                                    editor.apply()
                                    val intent =
                                        Intent(applicationContext, MainActivity::class.java)
                                    intent.putExtra(
                                        BALANCE,
                                        response.body()?.user!!.balance.toString()
                                    )
                                    intent.putExtra(PHONE, response.body()?.user!!.phone.toString())
                                    intent.putExtra(NAME, response.body()?.user!!.name.toString())
                                    intent.putExtra(EMAIL, response.body()?.user!!.email.toString())
                                    intent.putExtra(ID, response.body()?.user!!.id.toString())
                                    startActivity(intent)
                                    finish()
                                } else {
                                    Toast.makeText(
                                        applicationContext,
                                        "Please verified account before login",
                                        Toast.LENGTH_SHORT
                                    ).show()
                                }

                            } else {
                                Toast.makeText(
                                    applicationContext,
                                    "Account does not exist or has not been verified",
                                    Toast.LENGTH_SHORT
                                ).show()
                            }
                        }
                    })

                }


            }
        }

        tv_register_account.setOnClickListener {
            val intent = Intent(this, RegisterActivity::class.java)
            startActivity(intent)
        }

        tv_forgot_pasword.setOnClickListener {
            val intent = Intent(this, ForgotPassword::class.java)
            startActivity(intent)
        }

        tv_validate_account.setOnClickListener {
            val intent = Intent(this, LoginVerificationActiviry::class.java)
            startActivity(intent)
        }

//

    }

}
