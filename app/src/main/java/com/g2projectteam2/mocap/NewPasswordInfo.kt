package com.g2projectteam2.mocap

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_forgot_password.*
import kotlinx.android.synthetic.main.activity_new_password_info.*

class NewPasswordInfo : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_password_info)
        overridePendingTransition(R.anim.fade_in, R.anim.fade_out)

        overridePendingTransition(R.anim.fade_in,R.anim.fade_out)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        btn_ok_new_password.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            startActivity(intent)
            finish()

        }
    }
}
