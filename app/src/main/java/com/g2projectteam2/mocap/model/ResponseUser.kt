package com.g2projectteam2.mocap.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class ResponseUser(
	val users: List<UsersItem?>? = null,
	val status: String? = null
) : Parcelable

@Parcelize
data class UsersItem(
	val password: String? = null,
	val activeStatus: Boolean? = null,
	val balance: Int? = null,
	val phone: String? = null,
	val name: String? = null,
	val id: Int? = null,
	val loginStatus: Boolean? = null,
	val email: String? = null
) : Parcelable
