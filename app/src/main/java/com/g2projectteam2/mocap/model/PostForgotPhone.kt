package com.g2projectteam2.mocap.model

import com.google.gson.annotations.SerializedName

data class PostForgotPhone (
    @SerializedName("phone")
    val phone: String
)