package com.g2projectteam2.mocap.model

import com.google.gson.annotations.SerializedName

data class PostLoginEmail (
    @SerializedName("email")
    val email: String,
    @SerializedName("password")
    val password: String
)

