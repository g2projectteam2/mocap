package com.g2projectteam2.mocap.model

import com.google.gson.annotations.SerializedName

data class PostResendOTP  (
    @SerializedName("id")
    val id: Int
)