package com.g2projectteam2.mocap.model

import com.google.gson.annotations.SerializedName

data class PostLoginPhone (
    @SerializedName("phone")
    val phone: String,
    @SerializedName("password")
    val password: String
)
