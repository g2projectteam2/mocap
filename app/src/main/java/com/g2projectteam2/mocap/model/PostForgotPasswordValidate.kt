package com.g2projectteam2.mocap.model

import com.google.gson.annotations.SerializedName

data class PostForgotPasswordValidate (
    @SerializedName("id")
    val id: Int,
    @SerializedName("otp")
    val otp: String

)