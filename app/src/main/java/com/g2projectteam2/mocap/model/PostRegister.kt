package com.g2projectteam2.mocap.model

import com.google.gson.annotations.SerializedName

data class PostRegister(
    @SerializedName("email")
    val email: String,
    @SerializedName("password")
    val password: String,
    @SerializedName("phone")
    val phone: String,
    @SerializedName("name")
    val name: String


)