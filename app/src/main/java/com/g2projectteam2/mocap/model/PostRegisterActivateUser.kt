package com.g2projectteam2.mocap.model

import com.google.gson.annotations.SerializedName

data class PostRegisterActivateUser (
    @SerializedName("email")
    val email: String,
    @SerializedName("otp")
    val otp: String
)

