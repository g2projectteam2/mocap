package com.g2projectteam2.mocap.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class PostRegisterResponse(

	@field:SerializedName("description")
	val description: String? = null,

	@field:SerializedName("user")
	val user: User? = null,

	@field:SerializedName("status")
	val status: String? = null
) : Parcelable

@Parcelize
data class User(

	@field:SerializedName("password")
	val password: String? = null,

	@field:SerializedName("activeStatus")
	val activeStatus: Boolean? = null,

	@field:SerializedName("balance")
	val balance: Int? = null,

	@field:SerializedName("phone")
	val phone: String? = null,

	@field:SerializedName("name")
	val name: String? = null,

	@field:SerializedName("otp")
	val otp: String? = null,

	@field:SerializedName("id")
	val id: Int? = null,

	@field:SerializedName("loginStatus")
	val loginStatus: Boolean? = null,

	@field:SerializedName("email")
	val email: String? = null
) : Parcelable
