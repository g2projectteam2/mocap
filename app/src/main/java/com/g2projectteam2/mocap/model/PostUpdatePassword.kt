package com.g2projectteam2.mocap.model

import com.google.gson.annotations.SerializedName

class PostUpdatePassword (
    @SerializedName("id")
    val id: Int,
    @SerializedName("password")
    val password : String
)
