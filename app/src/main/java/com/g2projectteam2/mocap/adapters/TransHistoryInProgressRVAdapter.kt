package com.g2projectteam2.mocap.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.FragmentTransaction
import androidx.recyclerview.widget.RecyclerView
import com.g2projectteam2.mocap.LoginActivity
import com.g2projectteam2.mocap.LoginActivity.Companion.PHONE
import com.g2projectteam2.mocap.MainActivity
import com.g2projectteam2.mocap.MainActivity.Companion.BUNDLE_DATA_USER
import com.g2projectteam2.mocap.R
import com.g2projectteam2.mocap.api.APIClient
import com.g2projectteam2.mocap.api.APIInterface
import com.g2projectteam2.mocap.database.entity.TransHistoryInProgressEntity
import com.g2projectteam2.mocap.fragments.CreditMainFragment
import com.g2projectteam2.mocap.models.PostCancelTrans
import com.g2projectteam2.mocap.models.PostCancelTransResModel
import com.g2projectteam2.mocap.models.TransHistoryInProgressModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import org.json.JSONException
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.lang.reflect.Type

class TransHistoryInProgressRVAdapter :
    RecyclerView.Adapter<TransHistoryInProgressRVAdapter.ViewHolder>() {

    companion object {
        const val IS_FROM_HISTORY = "IS FROM HISTORY"
    }

    private var dataList =
        ArrayList<TransHistoryInProgressEntity>() //TODO tipe bisa ganti, tergantung backend
    private var context: Context? = null
    private var activity: AppCompatActivity? = null
    private var onItemCallback: OnItemCallback? = null

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val phoneNumber: TextView = itemView.findViewById(R.id.tv_history_in_progress_phone_number)
        val date: TextView = itemView.findViewById(R.id.tv_history_in_progress_date)
        val product: TextView = itemView.findViewById(R.id.tv_history_in_progress_product)
        val payment: TextView = itemView.findViewById(R.id.tv_history_in_progress_payment)
        val buttonPayNow: Button = itemView.findViewById(R.id.btn_pay_now_in_progress)
        val buttonCancel: Button = itemView.findViewById(R.id.btn_cancel_in_progress)
    }

    interface OnItemCallback {
        fun onItemClicked(transHistoryInProgressEntity: TransHistoryInProgressEntity)
    }

    fun setOnItemCallback(onItemCallback: OnItemCallback) {
        this.onItemCallback = onItemCallback
    }

    fun setDataList(dataList: ArrayList<TransHistoryInProgressEntity>) {
        this.dataList = dataList
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        activity = context as AppCompatActivity
        return ViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_list_trans_history_in_progress, parent, false)
        )
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.phoneNumber.text = dataList[position].phoneNumber
        holder.date.text = dataList[position].date
        holder.product.text = "${dataList[position].product}"
        holder.payment.text = dataList[position].totalPayment

        holder.buttonPayNow.setOnClickListener {
            val transHistoryData =
                TransHistoryInProgressModel(
                    holder.phoneNumber.text.toString(),
                    holder.date.text.toString(),
                    holder.product.text.toString(),
                    holder.payment.text.toString(),
                    dataList[position].id
                )

            saveTransInProgress(transHistoryData)
            activity?.supportFragmentManager?.popBackStack()
        }

        holder.buttonCancel.setOnClickListener {

            val sharedPreferences =
                context?.getSharedPreferences(BUNDLE_DATA_USER, Context.MODE_PRIVATE)
            val userPhoneNumber = sharedPreferences?.getString(PHONE, null)

            if (userPhoneNumber != null) {
                APIClient().getClient()?.create(APIInterface::class.java)
                    ?.postCancelTrans(PostCancelTrans(userPhoneNumber))
                    ?.enqueue(object : Callback<PostCancelTransResModel> {
                        override fun onFailure(call: Call<PostCancelTransResModel>, t: Throwable) {
                            Toast.makeText(context, "Connection issue.", Toast.LENGTH_SHORT)
                                .show()
                        }

                        override fun onResponse(
                            call: Call<PostCancelTransResModel>,
                            response: Response<PostCancelTransResModel>
                        ) {
                            if (response.isSuccessful) {
                                Toast.makeText(
                                    context,
                                    "Success canceling transaction",
                                    Toast.LENGTH_SHORT
                                ).show()
                            } else {
                                val jsonObject: JSONObject?
                                try {
                                    jsonObject = JSONObject(response.errorBody()!!.string())
                                    Log.d("TestingComplete", jsonObject.toString())
                                } catch (e: JSONException) {
                                    e.printStackTrace()
                                }
                            }
                        }
                    })

                val transHistoryData = TransHistoryInProgressEntity(
                    holder.phoneNumber.text.toString(),
                    holder.date.text.toString(),
                    holder.product.text.toString(),
                    holder.payment.text.toString()
                )
                val id = dataList[position].id
                transHistoryData.id = id
                val historyViewModel = MainActivity().getHistoryMView()
                historyViewModel.delete(transHistoryData)

            }
        }
    }

    private fun saveTransInProgress(dataTrans: TransHistoryInProgressModel) {
        val prefs = context?.getSharedPreferences(
            CreditMainFragment.TRANS_PENDING_DATA,
            Context.MODE_PRIVATE
        )
        val editor = prefs?.edit()
        val gson = Gson()
        val jsonDataTrans = gson.toJson(dataTrans)
        editor?.putString(CreditMainFragment.DATA_LIST_TRANS, jsonDataTrans)
        editor?.putBoolean(IS_FROM_HISTORY, true)
        editor?.apply()
    }
}