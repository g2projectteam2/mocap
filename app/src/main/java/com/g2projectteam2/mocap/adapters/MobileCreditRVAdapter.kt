package com.g2projectteam2.mocap.adapters

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.g2projectteam2.mocap.R
import com.g2projectteam2.mocap.models.Amount
import com.g2projectteam2.mocap.models.CreditModel
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.bottom_sheet_confirm.*

class MobileCreditRVAdapter: RecyclerView.Adapter<MobileCreditRVAdapter.ViewHolder>() {

    private var context: Context? = null
    private var activity: AppCompatActivity? = null
    private var dataList: List<Amount>? = null
    private var onItemCallback: OnItemCallback? = null

    interface OnItemCallback {
        fun onItemClicked(position: Int)
    }

    fun setOnItemCallback(onItemCallback: OnItemCallback){
        this.onItemCallback = onItemCallback
    }

    fun setDataList(dataList: List<Amount>){
        this.dataList = ArrayList(dataList)
        notifyDataSetChanged()
    }

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val creditNominal: TextView = itemView.findViewById(R.id.tv_item_list_credit)
        val creditPrice: TextView = itemView.findViewById(R.id.tv_item_list_price)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        activity = context as AppCompatActivity
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_list_mobile_credit, parent, false))
    }

    override fun getItemCount(): Int = dataList!!.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val credit = dataList?.get(position)?.amount.toString()
        when(credit.length){
            4 -> holder.creditNominal.text = "${credit.subSequence(0,1)}K"
            5 -> holder.creditNominal.text = "${credit.subSequence(0,2)}K"
            6 -> holder.creditNominal.text = "${credit.subSequence(0,3)}K"
        }

        holder.creditPrice.text = "Rp${dataList?.get(position)?.amount}"

        holder.itemView.setOnClickListener {
            onItemCallback?.onItemClicked(position)

            val bottomSheet = activity?.bottom_sheet_confirm
            val bottomSheetBehavior = BottomSheetBehavior.from(bottomSheet!!)
            bottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        }
    }
}