package com.g2projectteam2.mocap.adapters

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.g2projectteam2.mocap.fragments.CompletedFragment
import com.g2projectteam2.mocap.fragments.InProgressFragment

class TransactionHistoryVPAdapter(fragmentManager: FragmentManager): FragmentPagerAdapter(fragmentManager, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val pageList = listOf(
        InProgressFragment(),
        CompletedFragment()
    )

    override fun getItem(position: Int): Fragment = pageList[position]

    override fun getCount(): Int = pageList.size

    override fun getPageTitle(position: Int): CharSequence? {
        return when(position){
            0 -> "In Progress"
            else -> "Completed"
        }
    }

}