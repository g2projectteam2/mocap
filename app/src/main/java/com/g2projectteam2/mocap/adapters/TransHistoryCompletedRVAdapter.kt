package com.g2projectteam2.mocap.adapters

import android.app.Activity
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.g2projectteam2.mocap.R
import com.g2projectteam2.mocap.models.Transaction

class TransHistoryCompletedRVAdapter: RecyclerView.Adapter<TransHistoryCompletedRVAdapter.ViewHolder>() {

    private var dataList = ArrayList<Transaction>() //TODO tipe bisa ganti, tergantung backend
    private var context: Context? = null
    private var activity: Activity? = null

    class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        val phoneNumber: TextView = itemView.findViewById(R.id.tv_history_completed_phone_number)
        val date: TextView = itemView.findViewById(R.id.tv_history_completed_date)
        val product: TextView = itemView.findViewById(R.id.tv_history_completed_product)
        val payment: TextView = itemView.findViewById(R.id.tv_history_completed_payment)
        val status: TextView = itemView.findViewById(R.id.tv_history_completed_status)
    }

    fun setDataList(dataList: List<Transaction>){
        this.dataList = ArrayList(dataList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        activity = context as AppCompatActivity
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_list_trans_history_completed, parent, false))
    }

    override fun getItemCount(): Int = dataList.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.phoneNumber.text = dataList[position].destinationPhone
        holder.date.text = dataList[position].timestamp
        holder.product.text = dataList[position].description
        holder.payment.text = "Rp${dataList[position].amount}"
        if (dataList[position].status) {
            holder.status.text = "Payment Success"
            holder.status.setTextColor(context?.resources!!.getColor(R.color.green))
        } else {
            holder.status.text = "Payment Failed"
            holder.status.setTextColor(context?.resources!!.getColor(R.color.red))
        }
    }
}